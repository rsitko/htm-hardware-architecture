__kernel void sp_overlap(__global unsigned int *in_data, __global float *boost_data, __global unsigned int *out_data,
unsigned int N, __local unsigned int *shared_data, unsigned int min_overlap)
{
unsigned int lid = get_local_id(0);
unsigned int gid = get_global_id(0);
unsigned int group_id = get_group_id(0);

shared_data[lid] = (gid < N) ? in_data[ gid ] : 0;
barrier(CLK_LOCAL_MEM_FENCE);

unsigned int local_size;
for (local_size=1; local_size < get_local_size(0); local_size *=2)
{
    if ((lid % (2*local_size)) == 0)
    {
        shared_data[lid] = shared_data[lid] + shared_data[lid + local_size];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
}

if (lid == 0)
{
    out_data[group_id] = ((unsigned int)shared_data[0] > min_overlap) ?
            ceil(shared_data[0] * boost_data[group_id]) : 0;
}

}
