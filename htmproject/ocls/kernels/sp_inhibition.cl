// unpacks data from dense to sparse int representation
int unpack_to_int(int data, unsigned int position)
{
    return (data >> position) & 0x0001;
}

__kernel void sp_inhibition(__global unsigned int *in_data,
__global float *boost_data, __global unsigned int *out_data,
unsigned int N, __local unsigned int *local_mem, unsigned int min_overlap,
unsigned int number_of_columns, unsigned int inhibition_range, unsigned int selection_range)
{

    unsigned int i; // loop iterator
    int j; // loop iterator
    unsigned int current;
    unsigned int index;
    unsigned int counter = 0; // counts how many times (during comparison) current value is bigger than a data stored in the local memory
    unsigned int lid = get_local_id(0);
    unsigned int gid = get_global_id(0);
    unsigned int group_id = get_group_id(0);
    int minus_inhibition_range_2 = -floor(inhibition_range/2.0);
    int inhibition_range_2_open = minus_inhibition_range_2 + inhibition_range;
    unsigned int local_size = get_local_size(0);

    // bit disagregation
    //unsigned int global_mod_pos = gid % 32; // global modulo position

    /* Overlap starts here */

    local_mem[lid] = (gid < N) ? in_data[ gid ] : 0;
    //local_mem[lid] = (gid < N) ? unpack_to_int(in_data[gid/32], global_mod_pos) : 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    for (i=1; i < local_size; i*=2)
    {
        if ((lid % (2*i)) == 0)
        {
            local_mem[lid] = local_mem[lid] + local_mem[lid + i];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    if (lid == 0)
    {
        out_data[group_id] = ((unsigned int)local_mem[0] > min_overlap) ?
            ceil(local_mem[0] * boost_data[group_id]) : 0;
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    /* Inhibition starts here */

    current = out_data[group_id];

    // check inhibition only if column is active
    // but keep mem barriers out of conditions
    if (current > 0)
    {
        for (j = minus_inhibition_range_2 + lid; j < inhibition_range_2_open; j+=local_size)
        {
            index = (group_id + j + number_of_columns) % number_of_columns;
            if (current > out_data[index])
            {
                counter++;
            }
        }
        local_mem[lid] = counter;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    if (current > 0 && lid == 0)
    {
        // put in fancy fast summing here
        counter = 0;
        for (i = 0; i < local_size; i++) {
            counter += local_mem[i];
        }
    }

    barrier(CLK_GLOBAL_MEM_FENCE);

    if (current > 0 && lid == 0)
    {
        // transfer to global memory
        j = max(((int) inhibition_range - (int) selection_range), 0);
        // all active go in (j==0) or current column overlap is bigger than j others
        out_data[group_id] = (j == 0 || counter > j) ? out_data[group_id] : 0;
    }
}
