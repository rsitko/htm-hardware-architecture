from htmproject.interfaces.ocl import OCL
import pyopencl as cl
import numpy as np


class OclSpOverlap(OCL):
    """
    OpenCl implementation of spatial pooler overlap
    """
    def __init__(self, kernel_name, min_overlap):
        super(OclSpOverlap, self).__init__(kernel_name)
        self.__data_buf = None  # Host buffer for data to be sent to GPU
        self.__destination_buf = None  # Buffer for data to be received from GPU
        self.__local_mem = None  # GPU block memory reserved for each column separately
        self.__output_vector = None
        self.__min_overlap = np.uint32(min_overlap)

        self._host_to_dev_time = 0

    def __prepare_kernel(self, data, boost_coeff, work_group_size):
        """
        Prepares data structures for the kernel.
        :param data_size: size of vector to be reduced.
        :param work_group: number of input synapses.
        :return:
        """
        data = np.array(data).astype(np.uint32)
        self.__data_buf = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY, size=data.nbytes)
        self.__data_write_evt = cl.enqueue_write_buffer(self.queue, self.__data_buf, data)

        boost_coeff = np.array(boost_coeff).astype(np.float32)
        self.__boost_buf = cl.Buffer(self.ctx, cl.mem_flags.READ_ONLY, size=boost_coeff.nbytes)
        self.__boost_write_evt = cl.enqueue_write_buffer(self.queue, self.__boost_buf, boost_coeff)

        self.__output_vector = np.zeros(len(data) / work_group_size).astype(np.int32)
        self.__destination_buf = cl.Buffer(self.ctx,
                                           cl.mem_flags.WRITE_ONLY,
                                           self.__output_vector.nbytes)

        self.__local_mem = cl.LocalMemory(work_group_size * (data.nbytes/len(data)))

    def _get_dest_buf_out_vec(self):
        return self.__destination_buf, self.__output_vector

    def _execute_kernel(self, data, boost_coeff, work_group_size):
        self.__prepare_kernel(data, boost_coeff, work_group_size)
        return self.program.sp_overlap(
            self.queue, (len(data),), (work_group_size,),
            self.__data_buf, self.__boost_buf,
            self.__destination_buf, np.int32(len(data)),
            self.__local_mem, self.__min_overlap,
            wait_for=[self.__data_write_evt, self.__boost_write_evt])

    def _post_execute_kernel(self):
        if self._profile:
            self._host_to_dev_time += self.__data_write_evt.profile.end - self.__data_write_evt.profile.start
