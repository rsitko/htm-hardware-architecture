import numpy
import datetime
import os
import logging
import math
import operator
import warnings

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import ScalarFormatter
from mpl_toolkits.mplot3d import Axes3D

from htmproject.interfaces.visualizer.visualizer import Visualizer
from .visualizer_params import params as visualizer_params


class PlotVisualizer(Visualizer):
    """
    Plots results of the analyzer.
    """
    def __init__(self, *args, **kwargs):
        super(PlotVisualizer, self).__init__(*args, **kwargs)

        self._logger = logging.getLogger()

        matplotlib.rcParams.update({'font.size': 4})
        matplotlib.rcParams['ytick.major.pad'] = '7'

        self._config.add_default_params_section('visualizer.plot', visualizer_params)

        self.__output_dir = self._config.get('visualizer.plot.output_dir')
        if not os.path.exists(self.__output_dir):
            os.makedirs(self.__output_dir)

        layers_amount = int(self._config.get('core.htm.layers'))
        self.__layers_to_be_plotted = self._config.get('visualizer.plot.layers', range(0, layers_amount))

        self.__global_plots = self._config.get('visualizer.plot.global_plots')
        self.__chunk_plots = self._config.get('visualizer.plot.chunk_plots')
        self.__per_layer_plots = self._config.get('visualizer.plot.per_layer_plots')

        self.__thresholds = kwargs.get('thresholds', {})

        self.__plots_per_page = 2
        self.__current_subplot = 0
        self.__fmt = ScalarFormatter(useOffset=False, useMathText=True)
        self.__fmt.set_powerlimits((-3, 3))

    def visualize_complete(self, prefix='', iterations_offset=0):
        """
        Generates set of waveforms for every layer in pdf according to the chosen parameters in
        the config file.
        :return:
        """

        self._logger.info('Rendering visualizations...')

        complete_stats = self._analyzer.get_complete_stats()

        output_file = os.path.join(
            self.__output_dir,
            '%s_%s.pdf' % (datetime.datetime.now(), prefix)
        )

        with PdfPages(output_file) as pdf:
            self.__visualize_global_stats(pdf, complete_stats['global'], iterations_offset)
            self.__visualize_chunk_stats(pdf, iterations_offset)
            self.__visualize_per_layer_stats(pdf, complete_stats['per_layer'], iterations_offset)

        self._logger.info('Visualizations rendered')

    def __visualize_chunk_stats(self, pdf, iterations_offset):
        possible_plots = [
            'histograms',
            'class_histograms',
            'cumulative_class_histograms',
            'class_similarity',
            'inter_class_similarity',
            'chunk_histograms'
        ]

        plots = [k for k in possible_plots if self.__chunk_plots[k]]
        plots.sort(key=lambda x: self.__chunk_plots[x])

        for plot in plots:
            method_name = '_%s__visualize_%s' % (self.__class__.__name__, plot)
            getattr(self, method_name)(pdf, iterations_offset)

    def __visualize_histograms(self, pdf, _):
        self.__render_plot3d(
            pdf,
            self._analyzer.get_last_chunk_histograms(),
            'all basic histograms',
            y_label='chunks'
        )

    def __visualize_chunk_histograms(self, pdf, _):
        data = self._analyzer.get_chunk_histograms_by_name()
        chunk_names = data.keys()
        chunk_names.sort()
        for name in chunk_names:
            if data[name].shape[0] > 1:
                self.__render_plot3d(
                    pdf,
                    data[name],
                    'basic histograms for \'%s\'' % (os.path.splitext(os.path.basename(name))[0], ),
                    y_label='chunks'
                )

    def __visualize_class_histograms(self, pdf, _):
        if hasattr(self._analyzer, 'get_last_chunk_histograms_by_class'):
            class_hists = self._analyzer.get_last_chunk_histograms_by_class(True)
            data = class_hists[0]
            chunk_names = class_hists[1]
            class_names = data.keys()
            class_names.sort()
            for name in class_names:
                if data[name].shape[0] > 1:
                    self.__render_plot3d(
                        pdf,
                        data[name],
                        'class histograms for \'%s\'' % (name, ),
                        y_ticks=map(lambda x: os.path.splitext(os.path.basename(x))[0], chunk_names[name])
                    )

    def __visualize_cumulative_class_histograms(self, pdf, _):
        if hasattr(self._analyzer, 'get_last_chunk_histograms_by_class'):
            class_hists = self._analyzer.get_last_chunk_histograms_by_class(True)
            data = class_hists[0]
            class_names = data.keys()
            class_names.sort()
            for name in class_names:
                cumulative = numpy.sum(data[name], 0)
                fig = self.__prepare_figure()
                self.__add_plot(
                    cumulative,
                    'columns',
                    'cumulative class histogram for \'%s\'' % (name, ),
                    max_val=data[name].shape[0],
                    trend=False,
                    bar=True
                )
                self.__maybe_save_figure(fig, pdf)
            self.__close_current_subplot(pdf)

    def __get_chunk_iterations(self, iterations_offset):
        sizes = self._analyzer.get_chunk_sizes()
        set_size = self._analyzer.get_set_size()
        iterations = []
        tmp = 0

        for s in sizes:
            tmp += s
            if tmp >= set_size:
                iterations.append(iterations_offset + tmp)

        return numpy.array(iterations)

    def __visualize_class_similarity(self, pdf, iterations_offset):
        data = self._analyzer.get_class_similarity()
        per_class = data['class_similarity']

        # not enough data gathered
        if not len(per_class):
            return

        fig = None
        metrics = per_class[-1].values()[-1].keys()
        metrics.sort()
        iterations = self.__get_chunk_iterations(iterations_offset)

        self.__current_subplot = 0

        mean = None
        if 'mean_class_similarity' in data:
            mean = data['mean_class_similarity']

        for m in metrics:
            fig = self.__prepare_figure(fig)

            per_class_keys = per_class[-1].keys()
            per_class_values = []
            for i, stats in enumerate(per_class):
                stat = [
                    stats[k][m] for k in per_class_keys
                ]
                per_class_values.append(stat)

            self.__add_plot(per_class_values, 'iterations', '%s class similarity' % (m,), labels=per_class_keys,
                            max_val=True, iterations=iterations, threshold=self.__thresholds.get('class_similarity'),
                            mean=map(operator.itemgetter(m), mean) if mean is not None else None)

            self.__maybe_save_figure(fig, pdf)

        self.__close_current_subplot(pdf)

    def __visualize_inter_class_similarity(self, pdf, iterations_offset):
        if hasattr(self._analyzer, 'get_inter_class_similarity'):
            data = self._analyzer.get_inter_class_similarity()
            inter_class = data['inter_class_similarity']

            # not enough data gathered
            if not len(inter_class):
                return

            mean = None
            if 'mean_inter_class_similarity' in data:
                mean = data['mean_inter_class_similarity']

            fig = None
            metrics = inter_class[-1].values()[-1].values()[-1].keys()
            metrics.sort()

            iterations = self.__get_chunk_iterations(iterations_offset)

            self.__current_subplot = 0

            similarities = self.__extract_inter_class_similarities(inter_class)
            labels = self.__extract_inter_class_labels(inter_class[-1])

            for m in metrics:
                fig = self.__prepare_figure(fig)

                self.__add_plot(map(lambda sims: map(operator.itemgetter(m), sims), similarities), 'iterations',
                                '%s inter-class similarity' % (m,), labels=labels, max_val=True, iterations=iterations,
                                threshold=self.__thresholds.get('inter_class_similarity'),
                                mean=map(operator.itemgetter(m), mean))

                self.__maybe_save_figure(fig, pdf)

            self.__close_current_subplot(pdf)

    @staticmethod
    def __extract_inter_class_data(item, func, keys):
        results = []
        used = []
        for cn in keys:
            used.append(cn)
            for cin, cis in item[cn].items():
                if cin not in used:
                    results.append(func(cn, cin, cis))
        return results

    @staticmethod
    def __extract_inter_class_labels(item):
        keys = item.keys()
        keys.sort()
        return PlotVisualizer.__extract_inter_class_data(
            item,
            lambda cn, cin, _: '%s - %s' % (cn, cin),
            keys
        )

    @staticmethod
    def __extract_inter_class_similarities(inter_class):
        similarities = []
        keys = inter_class[-1].keys()
        keys.sort()
        for item in inter_class:
            similarities.append(PlotVisualizer.__extract_inter_class_data(
                item,
                lambda _a, _b, cis: cis,
                keys
            ))
        return similarities

    def __visualize_global_stats(self, pdf, global_stats, iterations_offset):
        self.__current_subplot = 0

        possible_plots = [
            'percent_of_active_inputs',
            'percent_of_active_outputs'
        ]

        plots = [k for k in possible_plots if self.__global_plots[k]]
        plots.sort(key=lambda x: self.__global_plots[x])

        for plot in plots:
            method_name = '_%s__visualize_%s' % (self.__class__.__name__, plot)
            getattr(self, method_name)(pdf, global_stats[plot], iterations_offset)

        self.__close_current_subplot(pdf)

    def __visualize_percent_of_active_inputs(self, pdf, data, iterations_offset):
        self.__render_plot(
            pdf,
            numpy.array(data),
            'percent_of_active_inputs',
            offset=iterations_offset
        )

    def __visualize_percent_of_active_outputs(self, pdf, data, iterations_offset):
        self.__render_plot(
            pdf,
            numpy.array(data),
            'percent_of_active_outputs',
            offset=iterations_offset
        )

    def __visualize_per_layer_stats(self, pdf, per_layer_stats, iterations_offset):
        plots = [k for k, v in self.__per_layer_plots.items() if v]

        # default plot order is alphabetical
        plots.sort()

        # but user can set his own by assigning indices > 0
        plots.sort(key=lambda x: self.__per_layer_plots[x])

        pages = int(math.ceil(float(len(plots))/self.__plots_per_page))

        for layer in self.__layers_to_be_plotted:
            self.__current_subplot = 0
            try:
                layer_stats = per_layer_stats[layer]
            except IndexError:
                self._logger.warn('No data for layer %i' % (layer, ))
            else:
                for i in xrange(pages):
                    fig = plt.figure()
                    fig.suptitle('Layer %i' % (layer, ))

                    for j in xrange(self.__plots_per_page):
                        try:
                            key = plots[i*self.__plots_per_page + j]
                            self.__render_plot(
                                pdf,
                                numpy.array(layer_stats[key]),
                                key,
                                fig,
                                offset=iterations_offset
                            )
                        except IndexError:
                            pass

                    self.__close_current_subplot(pdf, fig)

    def __render_plot3d(self, pdf, data, title, normalized=True, y_label=None, y_ticks=None):
        y_dim = data.shape[0]
        try:
            x_dim = data.shape[1]  # when second arg. of tuple is empty there is only one row
        except IndexError:
            self._logger.warning('%s plot: there is only 1 row - the minimum number is 2' % (title, ))
            x_dim = data.shape[0]  # there is only one arg. of the tuple - size of columns
            y_dim = 2  # set row to 2 for a sake of plot_surface

        x = numpy.arange(0, x_dim, 1)
        y = numpy.arange(0, y_dim, 1)
        z = data
        x_mesh, y_mesh = numpy.meshgrid(x, y)

        fig = plt.figure()
        fig.suptitle(title)
        ax = Axes3D(fig)
        ax.set_xlabel('HTM outputs')

        if y_label is not None:
            ax.set_ylabel(y_label)

        if y_ticks is not None:
            ax.set_yticklabels(y_ticks)
            ax.set_ylim3d((0, len(y_ticks)-1))

        if normalized:
            ax.set_zlim3d((0, 1))

        ax.plot_surface(x_mesh, y_mesh, z, cmap=plt.cm.jet, cstride=1, rstride=1)
        pdf.savefig(fig)
        plt.close()

    def __render_plot(self, pdf, data, title, fig=None, offset=0, x_label='iterations', max_val=False):
        if data[0] is None:
            self._logger.warning('\'%s\' plot cannot be rendered due to the lack of data' % (title, ))
            return

        fig = self.__prepare_figure(fig)
        self.__add_plot(data, x_label, title, max_val=max_val, offset=offset)
        self.__maybe_save_figure(fig, pdf)

    def __add_plot(self, data, x_label, y_label, trend=True, labels=None, max_val=False, offset=0, iterations=None,
                   threshold=None, mean=None, bar=False):
        iterations = iterations if iterations is not None else numpy.arange(
            offset,
            offset + (data.shape[0] if isinstance(data, numpy.ndarray) else len(data)),
            1
        )

        if threshold is not None:
            self.__add_threshold(iterations, threshold, max_val)

        if isinstance(data[0], tuple) or isinstance(data[0], list):
            if labels is None:
                labels = ['max', 'mean', 'min', 'variance']

            for i in xrange(len(data[0])):
                plt.plot(
                    iterations,
                    map(operator.itemgetter(i), data),
                    '-' if len(iterations) > 1 else 'x',
                    label=labels[i] if labels[i] != '_' else '_nolegend_'
                )
        else:
            if bar:
                plt.bar(iterations, data)
            else:
                plt.plot(
                    iterations,
                    data,
                    '-' if len(iterations) > 1 else 'x'
                )

        plt.xlabel(x_label, rotation=0)
        plt.ylabel(y_label, rotation=90)

        if max_val:
            max_val += 0.05
            plt.ylim([-0.05, max_val])

        if mean is not None:
            self.__add_mean(iterations, mean)

        if (labels is not None and len(labels) > 1) or mean is not None:
            plt.legend()

        if trend:
            self.__add_trend_line(iterations, data)

    def __maybe_save_figure(self, fig, pdf):
        if not self.__current_subplot:
            pdf.savefig(fig)
            plt.close('all')

    def __prepare_figure(self, fig=None):
        if fig is None:
            if not self.__current_subplot:
                fig = plt.figure()
            else:
                fig = plt.gcf()

        fig.add_subplot(self.__plots_per_page, 1, self.__current_subplot + 1)
        plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.2, hspace=0.4)
        plt.gca().yaxis.set_major_formatter(self.__fmt)

        self.__current_subplot = (self.__current_subplot + 1) % self.__plots_per_page

        return fig

    @staticmethod
    def __add_mean(iterations, mean):
        plt.plot(
            iterations,
            mean,
            '-' if len(iterations) > 1 else '.',
            label='mean'
        )

    @staticmethod
    def __add_trend_line(iterations, data):
        if isinstance(data[0], tuple) or isinstance(data[0], list):
            data = map(operator.itemgetter(-1), data)

        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                z = numpy.polyfit(iterations, data, 4)
                p = numpy.poly1d(z)
                plt.plot(iterations, p(iterations), 'r--')
            except (numpy.RankWarning, RuntimeWarning):
                pass

    @staticmethod
    def __add_threshold(iterations, threshold, normalized):
        if len(iterations) < 2:
            iterations = numpy.array([
                iterations[0] - 0.5,
                iterations[0],
                iterations[0] + 0.5
            ])
        if normalized or not threshold['above']:
            plt.fill_between(
                iterations,
                numpy.ones(iterations.shape) * threshold['threshold'],
                1.0 if threshold['above'] else 0.0,
                edgecolor='0.5',
                facecolor='0.9',
                linestyle=':',
                linewidth=0.5,
                label='_nolegend_'
            )
        else:
            plt.plot(
                iterations,
                numpy.ones(iterations.shape) * threshold['threshold'],
                color='0.5',
                linestyle=':',
                linewidth=0.5,
                label='_nolegend_'
            )
        plt.xlim(iterations[0], iterations[-1])

    def __close_current_subplot(self, pdf, fig=None):
        if self.__current_subplot:
            pdf.savefig(fig)
            plt.close('all')
            self.__current_subplot = 0
