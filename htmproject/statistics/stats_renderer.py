import copy
import glob
import json
import os
import warnings
import math
import numpy
import operator

import re

from htmproject.configs.json import JSONConfig
from htmproject.interfaces.cli import Dumper


class StatsRenderer(object):
    def __init__(
            self,
            param,
            begin=None,
            end=None,
            step=None,
            multiplier=None,
            results_path=None,
            visualization_path=None,

            dir_path=None,

            stats_files=None,
            instances=None,
            config_template=None
    ):
        self.__results = {}

        self.__param = param

        if stats_files is None:
            self.__stats_files = {}

        if dir_path is not None:
            self.__stats_files.update(self.__get_stats_files(dir_path))

        if begin is not None and end is not None:
            self.__start = begin
            self.__stop = end
        else:
            values = self.__stats_files.keys()
            values.sort()
            self.__start = values[0]
            self.__stop = values[-1]

        self.__step = step
        self.__multiplier = multiplier
        self.__instances = instances

        if results_path is not None:
            self.__results_path = results_path
        else:
            self.__results_path = 'results-%s-b-%s-e-%s.json' % (
                self.__param, self.__start, self.__stop)
        if visualization_path is not None:
            self.__visualization_path = visualization_path
        else:
            self.__visualization_path = 'visualization-%s-b-%s-e-%s.pdf' % (
                self.__param, self.__start, self.__stop)

        if config_template is None and dir_path is not None:
            self.__config_template = JSONConfig(os.path.join(dir_path, 'config.json'))
        else:
            self.__config_template = config_template

    def __get_stats_files(self, dir_path):
        paths = {}

        files = glob.glob(
            os.path.join(dir_path, 'stats-%s-*-*.json' % (self.__param, ))
        )
        for f in files:
            m = re.match('stats-[\w_.]+-([\d.]+)-[\d.]+\.json', os.path.basename(f), re.I)
            if m is not None:
                value = float(m.group(1))
                paths.setdefault(value, []).append(os.path.abspath(f))

        return paths

    def add_stats_file(self, param_value, stats_path):
        self.__stats_files.setdefault(param_value, []).append(stats_path)

    def render(self):
        for param_value in self.__stats_files.keys():
            self.__accumulate_stats(param_value)

        self.__dump_stats()

        if self.__visualization_path:
            self.__plot_stats()

    def __accumulate_stats(self, param_value):
        learning = []
        testing = []

        for sts in self.__stats_files[param_value]:
            with open(sts, 'r') as sts_fd:
                try:
                    result = json.load(sts_fd)
                except ValueError:
                    pass
                else:
                    if result['learning'] is not None:
                        learning.append(result['learning']['f1_score'])
                    if result['testing'] is not None:
                        testing.append(result['testing']['f1_score'])

        learning = numpy.array(learning)
        testing = numpy.array(testing)

        self.__results[param_value] = {
            'learning': {
                'mean': None,
                'variance': None
            },
            'testing': {
                'mean': None,
                'variance': None
            }
        }

        if len(learning):
            self.__results[param_value]['learning']['mean'] = learning.mean()
            self.__results[param_value]['learning']['variance'] = learning.var()

        if len(testing):
            self.__results[param_value]['testing']['mean'] = testing.mean()
            self.__results[param_value]['testing']['variance'] = testing.var()

    def __dump_stats(self):
        dumper = Dumper()

        dumper.dump(self.__results_path, self.__results)

    def __plot_stats(self):
        import matplotlib.pyplot as plt
        from matplotlib.backends.backend_pdf import PdfPages

        keys = self.__results.keys()
        keys.sort()

        learning_mean = self.__get_data(keys, 'learning', 'mean')
        testing_mean = self.__get_data(keys, 'testing', 'mean')
        learning_var = self.__get_data(keys, 'learning', 'variance')
        testing_var = self.__get_data(keys, 'testing', 'variance')

        suptitle = '%s from %s to %s' % (
            self.__param,
            self.__start,
            self.__stop
        )

        if self.__step is not None or self.__multiplier is not None:
            suptitle += ' with'
            if self.__step is not None:
                suptitle += ' step %s' % (
                    self.__step,
                )
                if self.__multiplier is not None:
                    suptitle += ' and'

            if self.__multiplier is not None:
                suptitle += ' multiplier %s' % (
                    self.__multiplier,
                )

        if self.__instances is not None:
            suptitle += ' (%s instances each)' % (
                self.__instances,
            )

        with PdfPages(self.__visualization_path) as pdf:
            fig = plt.figure(figsize=(11.69, 8.27), dpi=96)
            fig.suptitle(suptitle, y=0.95)
            fig.add_subplot(1, 1, 1)
            plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.2, hspace=0.4)

            if learning_mean[0] is not None:
                self.__add_plot_and_variance(keys, learning_mean, learning_var, 'learning', 'blue', plt)
            self.__add_plot_and_variance(keys, testing_mean, testing_var, 'testing', 'green', plt)

            if learning_mean[0] is not None:
                self.__add_trend_line(keys, learning_mean, plt)
            self.__add_trend_line(keys, testing_mean, plt)

            plt.legend(loc='best')
            plt.ylabel('F1-score')
            plt.xlabel(self.__param)

            plt.ylim((-0.05, 1.05))

            pdf.savefig(fig)
            plt.close('all')

            config = copy.deepcopy(self.__config_template)
            config[self.__param] = '{param}'
            config_json = json.dumps(config['all'], sort_keys=True, indent=4).split('\n')

            m = None
            for m in xrange(int(math.ceil(len(config_json) / 68.0))):
                if not m:
                    fig = plt.figure(figsize=(11.69, 8.27), dpi=96)
                fig.text(
                    0.07 + (m * 0.43),
                    0.95,
                    '\n'.join(config_json[m*68:(m+1)*68]),
                    size='xx-small',
                    verticalalignment='top'
                )
                if m:
                    pdf.savefig(fig)
            if not m and m is not None:
                pdf.savefig(fig)

    def __get_data(self, keys, set_name, data_key):
        return numpy.array([
            self.__results[k][set_name][data_key]
            for k in keys
        ])

    @staticmethod
    def __add_plot_and_variance(values, scores, var, label, color, plt):
        plt.plot(
            values,
            scores,
            'o',
            label=label,
            color=color
        )

        plt.fill_between(
            values,
            scores+var,
            scores-var,
            facecolor=color,
            alpha=0.5
        )

    @staticmethod
    def __add_trend_line(values, scores, plt):
        if isinstance(scores[0], tuple) or isinstance(scores[0], list):
            scores = map(operator.itemgetter(-1), scores)

        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                z = numpy.polyfit(values, scores, 4)
                p = numpy.poly1d(z)
                plt.plot(values, p(values), 'r--')
            except (numpy.RankWarning, RuntimeWarning):
                pass
