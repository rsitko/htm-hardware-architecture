#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import
import argparse
import sys

from htmproject.statistics.stats_getter import StatsGetter
from htmproject.statistics.stats_renderer import StatsRenderer


def add_common_args(parser):
    parser.add_argument(
        '-p',
        '--param',
        type=str,
        help='which config parameter to change during tests',
        default='core.htm.columns_per_layer.0'
    )

    parser.add_argument(
        '-b',
        '--begin',
        type=float,
        help='starting value for parameter'
    )

    parser.add_argument(
        '-e',
        '--end',
        type=float,
        help='end value for parameter'
    )

    parser.add_argument(
        '-s',
        '--step',
        type=float,
        help='step (increase) of the parameter'
    )

    parser.add_argument(
        '-m',
        '--multiplier',
        type=float,
        help='multiplier of the parameter',
        default=1
    )

    parser.add_argument(
        '-d',
        '--dump',
        type=argparse.FileType('w'),
        help='path to results JSON file'
    )

    viz_parser = parser.add_mutually_exclusive_group(required=False)
    viz_parser.add_argument(
        '-v',
        '--visualization',
        type=argparse.FileType('w'),
        help='path to results visualization file'
    )
    viz_parser.add_argument(
        '--no-visualization',
        dest='visualization',
        action='store_false'
    )
    parser.set_defaults(visualization=None)


def add_stats_args(parser):
    add_common_args(parser)

    parser.add_argument(
        'config',
        type=argparse.FileType('r'),
        help='config template file path',
        default='config.json'
    )

    parser.add_argument(
        '-n',
        '--instances',
        type=int,
        help='how many times to run HTM with single configuration',
        default=10
    )

    parser.add_argument(
        '-l',
        '--learning-set-size',
        type=int,
        help='desired learning set size',
        default=80
    )

    parser.add_argument(
        '-t',
        '--testing-set-size',
        type=int,
        help='desired testing set size',
        default=20
    )

    pickles_parser = parser.add_mutually_exclusive_group(required=False)
    pickles_parser.add_argument(
        '--save-pickles',
        dest='save_pickles',
        action='store_true',
        help='save htm & classifier pickles'
    )
    pickles_parser.set_defaults(save_pickles=False)

    pickles_parser.add_argument(
        '--use-pickles',
        dest='use_pickles',
        action='store_true',
        help='use htm & classifier pickles instead of running learning'
    )
    pickles_parser.set_defaults(use_pickles=False)

    parser.add_argument(
        '--max-processes',
        type=int,
        help='number of processor cores used',
        default=0
    )

    modular_parser = parser.add_mutually_exclusive_group(required=False)
    modular_parser.add_argument(
        '--modular',
        dest='modular',
        action='store_true',
        help='single HTM per each class'
    )
    modular_parser.add_argument(
        '--no-modular',
        dest='modular',
        action='store_false',
        help='single HTM for all classes'
    )
    parser.set_defaults(modular=True)

    parser.add_argument(
        '--instance-log',
        help='single instance logging level',
        choices=[
            'debug',
            'info',
            'warning',
            'error',
            'critical'
        ],
        default='info'
    )

    stats_parser = parser.add_mutually_exclusive_group(required=False)
    stats_parser.add_argument(
        '--instance-visualizations',
        dest='instance_visualizations',
        action='store_true',
        help='enable statistics visualization'
    )
    stats_parser.add_argument(
        '--no-instance-visualizations',
        dest='instance_visualizations',
        action='store_false',
        help='disable statistics visualization'
    )
    parser.set_defaults(instance_visualizations=False)

    histograms_parser = parser.add_mutually_exclusive_group(required=False)
    histograms_parser.add_argument(
        '--histograms',
        dest='histograms',
        action='store_true',
        help='make histograms dumps'
    )
    histograms_parser.add_argument(
        '--no-histograms',
        dest='histograms',
        action='store_false',
        help='don\'t make histograms dumps'
    )
    parser.set_defaults(histograms=False)


def add_recover_args(parser):
    add_common_args(parser)

    parser.add_argument(
        'dir',
        help='dir path containing generated stats files'
    )


def add_subparsers(subparsers):
    stats_parser = subparsers.add_parser('stats', help='perform statistical tests')
    add_stats_args(stats_parser)

    recover_parser = subparsers.add_parser('recover', help='save what can be saved after failed stats gathering')
    add_recover_args(recover_parser)


def parse_args(args):
    parser = argparse.ArgumentParser(description="HTM statistics gathering utility")

    add_stats_args(parser)

    return parser.parse_args(args)


def main(args):
    if args.mode == 'recover':
        recover(args)
    else:
        stats(args)


def recover(args):
    dump_path, visualization_path = get_dump_and_visualization_paths(args)

    sr = StatsRenderer(
        args.param,
        args.begin,
        args.end,
        args.step,
        args.multiplier,
        dump_path,
        visualization_path,
        dir_path=args.dir
    )
    sr.render()


def get_dump_and_visualization_paths(args):
    if args.dump is not None:
        args.dump.close()
        dump_path = args.dump.name
    else:
        dump_path = None

    if args.visualization is not None and args.visualization:
        args.visualization.close()
        visualization_path = args.visualization.name
    else:
        visualization_path = args.visualization

    return dump_path, visualization_path


def stats(args):
    args.config.close()

    dump_path, visualization_path = get_dump_and_visualization_paths(args)

    sg = StatsGetter(args.config.name, args.instances, args.param, args.begin, args.end, args.step, args.multiplier,
                     dump_path, visualization_path, args.learning_set_size, args.testing_set_size, args.save_pickles,
                     args.use_pickles, args.max_processes, args.modular, args.instance_log,
                     args.instance_visualizations, args.histograms)
    sg.run()


def run(args):
    main(args)

if __name__ == "__main__":
    run(parse_args(sys.argv[1:]))
