import copy
import logging
import os
import glob
import shutil
import subprocess
import time
import sys
import multiprocessing
import math
import operator
import itertools
import numpy

from htmproject.configs.json import JSONConfig
from htmproject.interfaces.cli import Dumper
from htmproject.interfaces.cli.cli_params import params as cli_params
from htmproject.statistics.stats_renderer import StatsRenderer

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG, stream=sys.stdout)


class StatsGetter(object):
    def __init__(self, config_path, instances, param, begin, end, step, multiplier, results_path, visualization_path,
                 learning_set_size=80, testing_set_size=20, save_pickles=False, use_pickles=False, max_processes=None,
                 modular=True, instance_log='info', instance_visualizations=False, histograms=False):
        self.__logger = logging.getLogger()

        self.__config_template = JSONConfig(config_path)
        self.__instances = instances
        self.__param = param
        self.__start = begin
        self.__stop = end
        self.__step = step
        self.__multiplier = multiplier
        self.__active_processes = []
        self.__processes_queue = []
        self.__results = {}
        self.__max_processes = int(math.floor(multiprocessing.cpu_count()*0.75)) if not max_processes else max_processes
        self.__results_path = results_path
        self.__visualization_path = visualization_path
        self.__save_pickles = save_pickles
        self.__use_pickles = use_pickles
        self.__modular = modular
        self.__instance_log = instance_log
        self.__instance_visualizations = instance_visualizations
        self.__dump_histograms = histograms

        self.__config_template.add_default_params_section('cli', cli_params)

        self.__learning_set_size = learning_set_size
        self.__testing_set_size = testing_set_size
        self.__learning_set_files = self.__get_files_list(self.__config_template['cli.learning_dir'])
        self.__testing_set_files = self.__get_files_list(self.__config_template['cli.testing_dir'])

        self.__renderer = StatsRenderer(
            self.__param,
            self.__start,
            self.__stop,
            self.__step,
            self.__multiplier,
            self.__results_path,
            self.__visualization_path,
            instances=self.__instances,
            config_template=self.__config_template
        )

    def run(self):
        param_values = []

        param_value = self.__start
        while param_value <= self.__stop:
            param_values.append(param_value)
            for i in xrange(self.__instances):
                self.__queue_instance(i, param_value)

            param_value += self.__step
            param_value *= self.__multiplier

        while self.__process_queue():
            time.sleep(5)

        self.__renderer.render()

    @staticmethod
    def __get_files_list(source_path):
        subdirs = [
            name for name in os.listdir(source_path) if os.path.isdir(os.path.join(source_path, name))
        ]
        files = {}
        for name in subdirs:
            files[name] = glob.glob(
                os.path.join(os.path.join(source_path, name), '*.*')
            )
        return files

    def __prepare_dir(self, source_set, size):
        dir_path = os.path.abspath(os.path.join(
            self.__config_template.get_root_path(),
            'set-%s' % (numpy.random.randint(2**32), )
        ))

        os.mkdir(dir_path, 0o700)

        for cls in source_set.keys():
            cls_dir = os.path.join(dir_path, cls)
            os.mkdir(cls_dir, 0o700)
            numpy.random.shuffle(source_set[cls])
            for sample in source_set[cls][:size]:
                os.symlink(sample, os.path.join(cls_dir, os.path.basename(sample)))

        return dir_path

    def __prepare_learning_dir(self):
        return self.__prepare_dir(
            self.__learning_set_files,
            self.__learning_set_size
        )

    def __prepare_testing_dir(self):
        return self.__prepare_dir(
            self.__testing_set_files,
            self.__testing_set_size
        )

    def __get_path_for_instance(self, name, param_value, instance, ext='json'):
        if ext:
            filename = '%s-%s-%s-%s.%s' % (name, self.__param, param_value, instance, ext)
        else:
            filename = '%s-%s-%s-%s' % (name, self.__param, param_value, instance)
        path = os.path.abspath(
            os.path.join(self.__config_template.get_root_path(), filename)
        )
        return path

    def __prepare_instance_data(self, param_value, instance):
        config = copy.deepcopy(self.__config_template)

        testing_dir = self.__prepare_testing_dir()
        learning_dir = None
        if not self.__use_pickles:
            learning_dir = self.__prepare_learning_dir()

        config[self.__param] = param_value
        config['cli.learning_dir'] = learning_dir
        config['cli.testing_dir'] = testing_dir

        config_path = self.__get_path_for_instance('config', param_value, instance)
        stats_path = self.__get_path_for_instance('stats', param_value, instance)
        htm_path = self.__get_path_for_instance('htm', param_value, instance, 'pickle')
        classifier_path = self.__get_path_for_instance('classifier', param_value, instance, 'pickle')
        out_path = self.__get_path_for_instance('out', param_value, instance, 'log')
        histograms_path = self.__get_path_for_instance('%s-histograms', param_value, instance)
        visualizer_dir = self.__get_path_for_instance('visualizations', param_value, instance, None)

        config['visualizer.plot.output_dir'] = visualizer_dir

        dumper = Dumper()
        dumper.dump(config_path, config['all'])

        return {
            'config_path': config_path,
            'stats_path': stats_path,
            'learning_dir': learning_dir,
            'testing_dir': testing_dir,
            'htm_pickle_path': htm_path,
            'classifier_pickle_path': classifier_path,
            'out_path': out_path,
            'histograms_path': histograms_path
        }

    def __clean_instance_data(self, process_data):
        os.remove(process_data['config'])

        if not self.__use_pickles:
            shutil.rmtree(process_data['learning_dir'])
        shutil.rmtree(process_data['testing_dir'])

    def __queue_instance(self, instance, param_value):
        data = self.__prepare_instance_data(param_value, instance)

        process_args = [
            'htmproject'
        ]

        if not self.__use_pickles:
            process_args.append('learn')

            if self.__save_pickles:
                process_args.append('--o-pickle=%s' % (data['htm_pickle_path'], ))
                process_args.append('--c-pickle=%s' % (data['classifier_pickle_path'], ))
        else:
            process_args.append('test')
            process_args.append('--i-pickle=%s' % (data['htm_pickle_path'],))
            process_args.append('--c-pickle=%s' % (data['classifier_pickle_path'],))

        process_args.append('--modular' if self.__modular else '--no-modular')
        process_args.append('--log=%s' % (self.__instance_log, ))
        process_args.append('--visualizations' if self.__instance_visualizations else '--no-visualizations')
        process_args.append('--c-stats=%s' % (data['stats_path'], ))

        if self.__dump_histograms:
            process_args.append('--histograms=%s' % (data['histograms_path'], ))

        process_args.append(data['config_path'])

        self.__processes_queue.append({
            'args': process_args,
            'config': data['config_path'],
            'learning_dir': data['learning_dir'],
            'testing_dir': data['testing_dir'],
            'stats': data['stats_path'],
            'ready': False,
            'process': None,
            'instance': instance,
            'param_value': param_value,
            'out': data['out_path']
        })

    def __process_queue(self):
        active_processes = []

        for item in self.__active_processes:
            if item['process'].poll() is not None:
                item['ready'] = True
                self.__logger.info('Instance %i. with config %s done' % (
                    item['instance'],
                    os.path.basename(item['config'])
                ))
                self.__renderer.add_stats_file(item['param_value'], item['stats'])
                self.__clean_instance_data(item)
            else:
                active_processes.append(item)
        self.__active_processes = active_processes

        to_run = itertools.ifilterfalse(operator.itemgetter('process'), self.__processes_queue)
        for i in xrange(len(self.__active_processes), self.__max_processes):
            try:
                item = to_run.next()
                out = open(item['out'], 'w')
                item['process'] = subprocess.Popen(item['args'], stdout=out, stderr=subprocess.STDOUT)
                self.__active_processes.append(item)
                self.__logger.info('Running %i. instance with config %s' % (
                    item['instance'],
                    os.path.basename(item['config'])
                ))
            except StopIteration:
                break

        if not len(self.__active_processes):
            return False

        return True
