#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run `python setup.py develop` which will install the command `htmproject`
inside your current environment.
"""
from __future__ import division, print_function, absolute_import
import argparse
import logging
import sys

from htmproject import __version__, cli, statistics

__author__ = "Maciej Wielgosz"
__copyright__ = "Maciej Wielgosz"
__license__ = "none"

logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s',
    level=logging.DEBUG,
    stream=sys.stdout,
    incremental=True
)


def parse_args(args):
    """
    Parse command line parameters

    :param args: command line parameters as list of strings
    :return: command line parameters as :obj:`argparse.Namespace`
    """
    parser = argparse.ArgumentParser(description="Hierarchical Temporal Memory (HTM) Python 2.7 implementation")

    parser.add_argument(
        '-v',
        '--version',
        action='version',
        version='HTMProject {ver}'.format(ver=__version__)
    )

    subparsers = parser.add_subparsers(dest='mode')

    cli.add_subparsers(subparsers)
    statistics.add_subparsers(subparsers)

    return parser.parse_args(args)


def main(args):
    """
    Runs project in the selected mode

    :param args: command line parameters as :obj:`argparse.Namespace`
    :return:
    """
    if args.mode in ['learn', 'test', 'work']:
        cli.run(args)
    elif args.mode in ['stats', 'recover']:
        statistics.run(args)


def run():
    args = parse_args(sys.argv[1:])
    main(args)

if __name__ == "__main__":
    run()
