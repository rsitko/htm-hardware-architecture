import json
import numpy

from htmproject.interfaces.iostreamer.reader import Reader
from .iostreamer_params import params as iostreamer


class JSONReader(Reader):
    """
    Reads from file to memory. To be optimized for large files.
    """
    def __init__(self, config):
        config.add_default_params_section('iostreamer', iostreamer)

        self.input_file = config.get("iostreamer.input_file")
        self.file = None
        self.content = []
        self.data_size = 0
        self.index = 0

    def get_data_item(self):
        item = numpy.array(self.content[self.index])
        self.index += 1
        if self.index == self.data_size:
            self.index = 0

        return item

    def open(self):
        self.file = open(self.input_file)
        self.content = json.load(self.file)
        self.data_size = len(self.content)
        self.index = 0

    def close(self):
        self.file.close()
