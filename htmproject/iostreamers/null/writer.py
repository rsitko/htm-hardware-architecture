from htmproject.interfaces.iostreamer.writer import Writer


class NullWriter(Writer):
    def save_data_item(self, item):
        pass

    def open(self):
        pass

    def close(self):
        pass
