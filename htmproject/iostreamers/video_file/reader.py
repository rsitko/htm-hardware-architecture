import os
import glob
import cv2
import numpy
import logging
import random

from htmproject.interfaces.iostreamer.reader import Reader
from htmproject.interfaces.iostreamer.exceptions import EmptyError
from .iostreamer_params import params as iostreamer


class VideoFileReader(Reader):
    """
    Reads frames from *.avi video files.
    """

    def __init__(self, config, shuffling=True):
        config.add_default_params_section('iostreamer', iostreamer)

        self._logger = logging.getLogger()

        self._capture = None
        self._video_files_list = None
        self._current_file = None
        self._shuffling = shuffling

        self._frames = self._read_video()
        self.__video_input_dir = config.get("iostreamer.video_frames_in_dir")
        self.__files_list = None

        if not os.path.exists(self.__video_input_dir):
            self._logger.critical("Input video directory '%s' does not exist" % (self.__video_input_dir, ))

    def _read_video(self):
        raise NotImplementedError()

    def get_data_item(self):
        """
        :rtype: numpy.ndarray
        :return: Video frame
        """
        return self._frames.next()

    def get_files_list(self):
        """
        :rtype: list
        :return: List of *.avi files in directory. Order of files does not need to match
            processing order
        """
        if self.__files_list is None:
            self.__files_list = glob.glob(
                os.path.join(self.__video_input_dir, '*.avi')
            ) + glob.glob(
                os.path.join(self.__video_input_dir, '**', '*.avi')
            )

        return self.__files_list

    def get_current_file(self):
        return self._current_file

    def get_data_set_size(self, files_filter=lambda n: True):
        size = 0
        files_list = self.get_files_list()

        for f in files_list:
            if files_filter(f):
                capture = cv2.VideoCapture(f)
                size += capture.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
                capture.release()

        return int(size)

    def open(self):
        """
        Read frames from all the files in the video folder.
        """

        files_list = self.get_files_list()

        if not len(files_list):
            self._logger.critical('There are no *.avi files in %s directory' % (self.__video_input_dir, ))
            raise EmptyError()

        if self._shuffling:
            random.shuffle(files_list)

        self._video_files_list = numpy.nditer([files_list])

    def close(self):
        """
        Close the video file.
        """
        if self._capture is not None:
            self._capture.release()

    def reset(self):
        """
        Resets (and potentially reshuffles) video files list
        """
        if not self._shuffling:
            self._video_files_list.reset()
        else:
            files_list = self.get_files_list()
            random.shuffle(files_list)
            self._video_files_list = numpy.nditer([files_list])

    def get_capture_prop(self, prop):
        return self._capture.get(prop)
