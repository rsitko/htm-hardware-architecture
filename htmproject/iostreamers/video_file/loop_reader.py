import cv2

from htmproject.iostreamers.video_file.reader import VideoFileReader


class VideoFileLoopReader(VideoFileReader):
    """
    Reads frames from *.avi video files in a loop
    """

    def _read_video(self):
        """

        :return:
        """
        frame = None
        files_ok = False

        while True:
            captured = False
            flag = True

            while not captured:
                while self._capture is None or not self._capture.isOpened() or not flag:
                    if self._capture is not None:
                        self._capture.release()
                        self._capture = None
                        flag = True
                    try:
                        self._current_file = str(self._video_files_list.next())
                        self._capture = cv2.VideoCapture(self._current_file)
                    except StopIteration:
                        if files_ok:
                            self.reset()
                        else:
                            raise StopIteration()

                flag, frame = self._capture.read()

                captured = flag
            files_ok = True

            yield frame
