import os

from htmproject.interfaces.mapper import Mapper


class FileNamesMapper(Mapper):
    @staticmethod
    def extract_name(v):
        v = os.path.basename(v)
        return v[:v.index('-')]

    @staticmethod
    def names_from_list(list_to_extract, unique=True):
        names = [
            FileNamesMapper.extract_name(v) for v in list_to_extract
        ]
        if unique:
            return list(set(names))
        else:
            return names

    def map(self, list_to_map):
        return [
            self._names.index(FileNamesMapper.extract_name(v)) for v in list_to_map
        ]
