import json
import os

from htmproject.configs.root import RootConfig


class JSONConfig(RootConfig):
    """

    """

    def __init__(self, config_file):
        config_handle = open(config_file)

        super(JSONConfig, self).__init__(os.path.dirname(config_file), json.load(config_handle))

        config_handle.close()
