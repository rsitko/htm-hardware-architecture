import logging
from time import time

logger = logging.getLogger()


class GlobalTimeExecutionAccumulator(object):
    """

    """
    def __init__(self):
        self.__total_time = 0
        self.__average_time = 0
        self.__measurements_counter = 0  # counts a number of time measurements

    def accumulate_time(self, current_time):
        self.__measurements_counter += 1
        self.__total_time += current_time

    def get_total_time(self):
        return self.__total_time

    def get_average_time(self):
        return float(self.__total_time) / self.__measurements_counter


global_time = GlobalTimeExecutionAccumulator()


def time_it(func):
    def func_wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        global_time.accumulate_time(end-start)
        #logger.debug("%s execution time: %f" % (func.__name__, end-start))
        logger.debug("%s average time %f " % (func.__name__, global_time.get_average_time()))
        return result
    return func_wrapper
