import numpy


class WrappedQueue(object):
    def __init__(self, size, dtype=bool):
        self.__size = size
        self.__data = numpy.ndarray((size, ), dtype=dtype)
        self.__index = 0

    def get_size(self):
        return self.__size

    def push(self, v):
        self.__data[self.__index] = v
        self.__index = (self.__index + 1) % self.__size

    def update(self, v):
        prev = (self.__size + self.__index - 1) % self.__size
        self.__data[prev] = v

    def get_set(self, copy=False):
        """
        :param bool copy: should data be copied? (use if you want to modify them)
        :rtype: numpy.ndarray
        :return: data in queue ignoring their adding order
        """
        return numpy.copy(self.__data) if copy else self.__data

    def get_array(self):
        """
        :rtype: numpy.ndarray
        :return: copy of data in queue respecting their adding order
        """
        return numpy.concatenate((self.__data[self.__index:], self.__data[:self.__index]))
