import logging


class RingBuffer(object):
    """
    :param [] input_buffer: input list to be turned into a ring buffer
    :param int start: location of the start element (start < size)
    :param int width: width of interest (width < size)
    """
    def __init__(self, input_buffer, start, width):

        self.size = len(input_buffer)
        self.start = start

        self.__logger = logging.getLogger()

        if width >= self.size/2:
            # self.__logger.debug(
            #     "Ring buffer width is too large. It has been reduced to len(buffer)/2 - 1: %i" % (self.size/2 - 1, )
            # )
            self.width = self.size/2 - 1
        else:
            self.width = width

        self.rotated_buffer = self.__initial_rotation(input_buffer)

    def __initial_rotation(self, input_buffer):
        """

        :param [] input_buffer: input list to be calibrated according to size, width
        :return: reduced_buffer
        :rtype: []
        """

        offset = len(input_buffer)
        long_buffer = 3*input_buffer
        left = long_buffer[self.start - self.width + offset:self.start + offset]
        right = long_buffer[self.start + offset:self.start + self.width + offset + 1]

        return left + right

    def rotate(self, number_of_iterations):
        """
        Rotates the buffer left by number_of_iterations.

        :param int number_of_iterations:
        """

        for i in range(number_of_iterations):
            dropped_first_item = self.rotated_buffer.pop(0)
            self.rotated_buffer.append(dropped_first_item)

    def get_rotated_buffer(self):
        """
        Function returns rotated buffer after each iteration.

        :rtype: []
        """

        return list(self.rotated_buffer)







