from htmproject.interfaces.encoder import Encoder


class PassThroughEncoder(Encoder):
    """
    Maps from the "normal" space to the multidimensional binary space
    """
    def __init__(self, config):
        """
        :param config: Configuration object
        """
        self.input_length = config.get("encoder.input_length")

    def get_binary_space_size(self):
        """

        :return: size of the binary space
        :rtype: int
        """

        return self.input_length

    def encode(self, item):
        """
        Maps item to SDR.
        :param numpy.array item:
        :return: SDR of item
        :rtype: numpy.array
        """

        return item > 0  # change input to bool
