import cv2
import numpy as np
import math
from htmproject.interfaces.encoder import Encoder
from .encoder_params import params as encoder


class VideoEncoder(Encoder):

    def __init__(self, config, reduced=False):
        config.add_default_params_section('encoder', encoder)

        self.reduced = reduced
        self.reduced_factor = 16
        frame_dim = config.get("encoder.video_frame_size")
        self.frame_size = frame_dim[0]*frame_dim[1]
        self.image_quantization_levels = int(config.get("encoder.image_quantization_levels"))
        frame_size_0 = int(math.floor(frame_dim[0]/self.reduced_factor))
        frame_size_1 = int(math.floor(frame_dim[1]/self.reduced_factor))
        self.reduced_frame_size = frame_size_0*frame_size_1
        self.lut = self.__lut_gen__()
        self.lut_htm = self.__lut_binary__()

    def __lut_gen__(self):
        """
        self.image_quantization_levels - amount of levels to be used for quantization
        It is assumed that gray scale 8-bit is used.
        """
        lut = []

        range_len = np.ceil(256/self.image_quantization_levels)

        for i in range(self.image_quantization_levels):
            lut += list(np.zeros(range_len, dtype=np.uint8) + i)

        lut = lut[:256]

        return np.array(lut)

    def __lut_binary__(self):
        """
        Used to convert pixels to the reduced representation
        """
        lut = np.ones((self.image_quantization_levels, self.image_quantization_levels-1), dtype=bool)
        lut = np.tril(lut, -1)
        return lut

    def __mapper__(self, frame, binary_lut):
        """
        Maps frame to binary representation.
        Example:
        For 8 bit HTM input and 3 value the output is 11100000
        For 8 bit HTM input and 5 value the output is 11111000
        :param frame numpy array: input image frame
        :param binary_lut numpy array: mapping scheme generated with __lut_binary__ method
        :return:
        """
        a = binary_lut.shape[1]
        out = np.zeros(a * frame.size, dtype=bool)

        for index, value in enumerate(frame.flatten()):
            out[index * a: (index + 1) * a] = binary_lut[value]

        return out

    def __transform_frame__(self, frame):
        """

        :param frame:
        :return:
        """
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # gray_eq = cv2.equalizeHist(gray)
        gray_lut = cv2.LUT(gray, self.lut)
        gray_binary = self.__mapper__(gray_lut, self.lut_htm)
        return gray_binary

    def encode(self, frame):
        """
        Encoding procedure.
        :param frame: video frame to be encoded.
        :return:
        """
        if self.reduced:
            frame = cv2.resize(frame, (frame.shape[1]/self.reduced_factor, frame.shape[0]/self.reduced_factor))

        frame = self.__transform_frame__(frame)

        return frame

    def get_binary_space_size(self):
        return (self.image_quantization_levels - 1) * (self.reduced_frame_size if self.reduced else self.frame_size)



