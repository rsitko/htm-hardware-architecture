import cv2
import math
from htmproject.interfaces.encoder import Encoder
from .encoder_params import params as encoder


class AdaptiveVideoEncoder(Encoder):

    def __init__(self, config, reduced=False):
        config.add_default_params_section('encoder', encoder)

        self.reduced = reduced
        self.reduced_factor = config.get("encoder.reduction_rate")

        if not self.reduced_factor or self.reduced_factor == 1:
            self.reduced = False

        self.frame_size = config.get("encoder.video_frame_size")
        self.reduced_frame_size = (
            int(math.floor(self.frame_size[0]/self.reduced_factor)),
            int(math.floor(self.frame_size[1]/self.reduced_factor))
        )

    def encode(self, frame):
        """
        Encoding procedure.
        :param frame: video frame to be encoded.
        :return:
        """
        if self.reduced:
            frame = cv2.resize(frame, self.reduced_frame_size)

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        th3 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
        th3_neg = 255 - th3
        frame = th3_neg.flatten() > 0
        return frame

    def get_binary_space_size(self):
        size = self.reduced_frame_size if self.reduced else self.frame_size
        return size[0]*size[1]



