import numpy
import operator

from htmproject.interfaces.analyzer import Analyzer


class BasicAnalyzer(Analyzer):
    def __init__(self, *args, **kwargs):
        super(BasicAnalyzer, self).__init__(*args, **kwargs)
        self._reset_history()

    def _reset_history(self):
        self.__chunk_outputs = []
        self.__chunk_info = []

        self.__global_stats_history = {
            'percent_of_active_inputs': [],
            'percent_of_active_outputs': []
        }

    def start(self):
        """
        Tells analyzer to wipe out possible old state and start accumulating stats

        """
        self._reset_history()

    def stop(self):
        """
        Tells analyzer to stop accumulating data and make final calculations

        """
        pass

    def save_iteration_stats(self, encoded_input, encoded_output):
        """
        Tells analyzer that iteration has passed an it needs to update its internal state

        """
        # collect data for basic histograms
        self.__chunk_outputs.append(encoded_output)

        # update global percentage stats
        self.__global_stats_history['percent_of_active_inputs'].append(
            self.__get_active_inputs(encoded_input)
        )
        self.__global_stats_history['percent_of_active_outputs'].append(
            self.__get_active_outputs(encoded_output)
        )

    def reset_chunk_outputs(self):
        """
        Resets the previous collected outputs history.

        """
        self.__chunk_outputs = []

    def save_chunk_info(self, chunk_name, size=None):
        histogram, size = self.__calculate_chunk_histogram(size)
        info = {
            'histogram': histogram,
            'name': chunk_name,
            'size': size
        }
        self.__chunk_info.append(info)
        return info

    def get_chunk_info(self):
        return self.__chunk_info

    def get_chunk_names(self):
        return numpy.array(map(operator.itemgetter('name'), self.__chunk_info))

    def get_chunk_histograms(self):
        return numpy.array(map(operator.itemgetter('histogram'), self.__chunk_info))

    def get_chunk_sizes(self):
        return numpy.array(map(operator.itemgetter('size'), self.__chunk_info))

    def get_chunk_histograms_by_name(self):
        histograms_by_name = {}
        data = self.__chunk_info
        unique_chunk_names = list(set(map(
            operator.itemgetter('name'),
            data
        )))
        for name in unique_chunk_names:
            histograms_by_name[name] = numpy.array(map(operator.itemgetter('histogram'), filter(
                lambda x: x['name'] == name,
                data
            )))
        return histograms_by_name

    def get_last_chunk_histograms(self):
        last_histograms = []

        def saver(_, histogram):
            last_histograms.append(histogram)

        self.__get_last_chunk_histograms(saver)

        return numpy.array(last_histograms)

    def get_last_chunk_histograms_by_name(self):
        last_histograms_by_name = {}

        def saver(chunk_name, histogram):
            last_histograms_by_name[chunk_name] = histogram

        self.__get_last_chunk_histograms(saver)

        return last_histograms_by_name

    def get_iteration_stats(self):
        """
        Returns statistics from the last iteration.

        :rtype: dict
        :return dict of dicts: Current set of statistics
        """
        global_stats = {}

        for k, v in self.__global_stats_history.items():
            global_stats[k] = v[-1]

        return {
            'global': global_stats
        }

    def get_complete_stats(self):
        """
        Returns a complete set of statistics from all the iterations.

        :rtype: dict
        :return dict of dicts: Complete set of statistics
        """
        return {
            'global': self.__global_stats_history
        }

    @staticmethod
    def _count_non_zero_in_percent(data):
        """
        Counts number of ones in the input numpy vector.

        :param data numpy.ndarray:
        :return float: % of ones in data (numpy array).
        """
        return (numpy.count_nonzero(data)/float(data.size))*100

    def __get_active_inputs(self, input_data):
        """
        Returns number of ones in input_data.

        :param input_data numpy.ndarray:
        :return float: % of active bits in input_data.
        """
        return self._count_non_zero_in_percent(input_data)

    def __get_active_outputs(self, output_data):
        """
        Returns number of ones in output_data.

        :param output_data numpy.ndarray:
        :return float: % of active bits in output_data.
        """
        return self._count_non_zero_in_percent(output_data)

    def __calculate_chunk_histogram(self, size=None):
        """
        Returns histogram of last iterations.

        :param size: How many last outputs to use when calculating histogram
        :rtype: numpy.ndarray
        :return: Calculated histogram
        """
        size = size if size is not None else len(self.__chunk_outputs)

        if size <= len(self.__chunk_outputs):
            return numpy.sum(numpy.array(self.__chunk_outputs[-size:]), axis=0) / float(size), size
        else:
            raise ValueError("Demanded vector longer than a length of the collected outputs.")

    def __get_last_chunk_histograms(self, output_saver):
        data = self.__chunk_info
        data_len = len(data)
        all_chunk_names_reversed = map(
            operator.itemgetter('name'),
            data
        )
        all_chunk_names_reversed.reverse()
        unique_chunk_names = list(set(all_chunk_names_reversed))
        for name in unique_chunk_names:
            i = data_len - all_chunk_names_reversed.index(name) - 1
            output_saver(name, numpy.array(data[i]['histogram']))
