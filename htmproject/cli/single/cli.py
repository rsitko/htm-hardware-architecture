import copy

from htmproject.classifiers.sklearn import SVMClassifier
from htmproject.interfaces.cli import CLI, HTMWrapper, Dumper, Pickler
from htmproject.mappers.filenames import FileNamesMapper
from htmproject.visualizers.plot import PlotVisualizer
from htmproject.interfaces.cli.exceptions import TooManyIterationsError


class SingleCLI(CLI):
    def __init__(self, *args, **kwargs):
        super(SingleCLI, self).__init__(*args, **kwargs)

        self._logger.info('CLI ENGINE: SINGLE')

    def __setup_htm_module(self):
        self.__htm_module = None
        self.__load_htm_module()
        if self.__htm_module is None:
            self.__create_htm_module()

    def __load_htm_module(self):
        if self._input_pickle_path is not None:
            self._logger.info('Loading trained HTM module...')
            try:
                self.__htm_module = Pickler.load(self._input_pickle_path)
                self.__htm_module.toggle_stats(self._enable_stats)
                self.__htm_module.toggle_reporter(self._enable_reporter)
                self.__htm_module.toggle_progress(self._enable_progress)
                self.__htm_module.set_partial_stats_frequency(self._partial_stats_frequency)
                self.__htm_module.update_config(copy.deepcopy(self._config))
                self._logger.info('HTM module loaded')
            except EOFError:
                self._logger.warning('Empty or corrupted pickle file. Creating new HTM module instead.')

    def __create_htm_module(self):
        self.__htm_module = HTMWrapper(
            config=self._config,
            enable_stats=self._enable_stats,
            partial_stats_frequency=self._partial_stats_frequency,
            enable_reporter=self._enable_reporter,
            enable_progress=self._enable_progress,
            single_mode=True
        )

    def _setup_learning(self, *args, **kwargs):
        super(SingleCLI, self)._setup_learning(*args, **kwargs)

        self.__setup_htm_module()

    def _setup_testing(self, *args, **kwargs):
        super(SingleCLI, self)._setup_testing(*args, **kwargs)

        self.__setup_htm_module()

    def _start_learning(self):
        """
        Learning mode flow
        """
        need_training = True
        can_learn = True
        classifier = None
        learning_stats = None
        testing_stats = None
        skip_testing = False

        while can_learn and need_training:
            can_learn = self.__train_module()
            skip_testing, learning_stats, classifier = self.__train_classifier()
            if not skip_testing:
                need_training, testing_stats = self.__test_module(classifier)

        # run test even after unsuccessful training
        if skip_testing:
            _, testing_stats = self.__test_module(classifier)

        self._dump_classifier_stats(learning_stats, testing_stats)
        self._save_state(classifier)

    def _start_testing(self):
        """
        Testing mode flow
        """
        if self._classifier_pickle_path is not None:
            self._logger.info('Loading classifier...')
            classifier = Pickler.load(self._classifier_pickle_path)
            self._logger.info('Classifier loaded')

            _, classifier_stats = self.__test_module(classifier, True)
            self._dump_classifier_stats(testing_stats=classifier_stats)
        else:
            testing_results = self.__get_testing_results(self._testing_dir, 'testing')
            self.__dump_histograms(testing_results)

    def _start_working(self):
        pass

    def __get_testing_results(self, testing_dir, set_name):
        self._logger.info('Gathering testing results for %s set...' % (set_name,))

        results = self.__htm_module.test(
            testing_dir,
            self._class_similarity_threshold,
            self._inter_class_similarity_threshold,
            visualizers=(PlotVisualizer,)
        )

        self._logger.info('Testing results gathered')

        return results

    def __train_module(self):
        """
        HTM learning mode
        """
        try:
            self.__htm_module.learn(
                self._learning_dir,
                self._testing_dir,
                self._max_learning_iterations,
                self._class_similarity_threshold,
                self._inter_class_similarity_threshold,
                visualizers=(PlotVisualizer, )
            )
        except TooManyIterationsError:
            return False

        return True

    def __get_testing_set(self, testing_dir, set_name, with_dump=False):
        results = self.__get_testing_results(testing_dir, set_name)

        if with_dump:
            self.__dump_histograms(results)

        chunk_names = results['histograms'].keys()
        mapper = FileNamesMapper(FileNamesMapper.names_from_list(chunk_names))

        histograms = results['histograms'].values()
        mapped_classes = mapper.map(chunk_names)

        return {
            'histograms': histograms,
            'mapped_classes': mapped_classes,
            'class_names': mapper.get_names()
        }

    def __train_classifier(self):
        """
        :rtype: SVMClassifier|None
        :return: Training result, statistics & trained classifier
        """
        results = self.__get_testing_set(self._learning_dir, 'learning')

        self._logger.info('Training classifier...')

        classifier = SVMClassifier()
        classifier.train(
            results['histograms'],
            results['mapped_classes']
        )

        self._logger.info('Classifier trained')

        stats = self._get_classifier_stats(classifier, results['histograms'], results['mapped_classes'],
                                           results['class_names'], 'learning')

        if stats['f1_score'] < self._f1_score_threshold:
            return True, stats, classifier

        return False, stats, classifier

    def __test_module(self, classifier, with_dump=False):
        """
        HTM testing mode
        :rtype: bool
        :return: Does HTM still need learning?
        """
        results = self.__get_testing_set(self._testing_dir, 'testing', with_dump)
        stats = self._get_classifier_stats(classifier, results['histograms'], results['mapped_classes'],
                                           results['class_names'], 'testing')

        if stats['f1_score'] > self._f1_score_threshold:
            return False, stats

        return True, stats

    def _save_state(self, classifier):
        self.__save_htm_module()
        self._save_classifier(classifier)

    def __save_htm_module(self):
        if self._output_pickle_path is not None:
            self._logger.info('Saving trained HTM module...')
            Pickler.dump(self._output_pickle_path, self.__htm_module)
            self._logger.info('HTM module saved')

    def __dump_histograms(self, testing_results):
        dumper = Dumper()

        dumper.dump(
            self._histograms_path,
            testing_results
        )
