#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import
import argparse
import sys
import logging

from pycallgraph import PyCallGraph
from pycallgraph.output import GraphvizOutput
from pycallgraph import Config
import datetime

from htmproject.cli.single import SingleCLI
from htmproject.cli.modular import ModularCLI


def add_common_args(parser):
    parser.add_argument(
        'config',
        type=argparse.FileType('r'),
        help='config file path',
        default='config.json'
    )

    engine_parser = parser.add_argument_group('CLI engine')

    modular_parser = engine_parser.add_mutually_exclusive_group(required=False)
    modular_parser.add_argument(
        '--modular',
        dest='modular',
        action='store_true',
        help='single HTM per each class'
    )
    modular_parser.add_argument(
        '--no-modular',
        dest='modular',
        action='store_false',
        help='single HTM for all classes'
    )
    engine_parser.set_defaults(modular=False)

    debug_parser = parser.add_argument_group('debug')

    progress_parser = debug_parser.add_mutually_exclusive_group(required=False)
    progress_parser.add_argument(
        '--progress',
        dest='progress',
        action='store_true',
        help='show per-iteration progress marks'
    )
    progress_parser.add_argument(
        '--no-progress',
        dest='progress',
        action='store_false',
        help='do not show per-iteration progress marks'
    )
    debug_parser.set_defaults(progress=True)

    debug_parser.add_argument(
        '-l',
        '--log',
        help='logging level',
        choices=[
            'debug',
            'info',
            'warning',
            'error',
            'critical'
        ],
        default='debug'
    )

    profile_parser = debug_parser.add_mutually_exclusive_group(required=False)
    profile_parser.add_argument(
        '--profile',
        dest='profile',
        action='store_true',
        help='generate pycallgraph output'
    )
    profile_parser.add_argument(
        '--no-profile',
        dest='profile',
        action='store_false',
        help='do not generate pycallgraph output'
    )
    debug_parser.set_defaults(profile=False)

    visualization_parser = parser.add_argument_group('visualizations')

    stats_parser = visualization_parser.add_mutually_exclusive_group(required=False)
    stats_parser.add_argument(
        '--visualizations',
        dest='visualizations',
        action='store_true',
        help='enable statistics visualization'
    )
    stats_parser.add_argument(
        '--no-visualizations',
        dest='visualizations',
        action='store_false',
        help='disable statistics visualization'
    )
    visualization_parser.set_defaults(visualizations=True)

    return visualization_parser


def add_learning_args(parser):
    visualization_parser = add_common_args(parser)

    visualization_parser.add_argument(
        '--partial-stats',
        type=int,
        help='partial statistics frequency (every n iterations); statistics visualization needs to be enabled',
        default=1000
    )

    parser.add_argument(
        '-i',
        '--iterations',
        type=int,
        help='maximum amount of iterations per HTM module in learning mode. 0 means single pass of learning set',
        default=0
    )

    rep_parser = parser.add_argument_group('reporting')

    reporter_parser = rep_parser.add_mutually_exclusive_group(required=False)
    reporter_parser.add_argument(
        '--reporter',
        dest='reporter',
        action='store_true',
        help='enable iterations reporter'
    )
    reporter_parser.add_argument(
        '--no-reporter',
        dest='reporter',
        action='store_false',
        help='disable iterations reporter'
    )
    rep_parser.set_defaults(reporter=False)

    learn_pickle_parser = parser.add_argument_group('pickles')

    learn_pickle_parser.add_argument(
        '--i-pickle',
        type=argparse.FileType('rb'),
        help='HTM module(s) input pickle file path'
    )

    learn_pickle_parser.add_argument(
        '--o-pickle',
        type=argparse.FileType('ab'),
        help='HTM module(s) output pickle file path'
    )

    learn_pickle_parser.add_argument(
        '--c-pickle',
        type=argparse.FileType('ab'),
        help='Classifier output pickle file path'
    )

    dumps_parser = parser.add_argument_group('data dumping')

    dumps_parser.add_argument(
        '--c-stats',
        type=argparse.FileType('w'),
        help='classifier stats dump JSON file path'
    )


def add_testing_args(parser):
    add_common_args(parser)

    parser.add_argument(
        '--testing-dir',
        help='overwritten testing dir path'
    )

    test_pickle_parser = parser.add_argument_group('pickles')

    test_pickle_parser.add_argument(
        '--i-pickle',
        type=argparse.FileType('rb'),
        help='HTM module(s) input pickle file path',
        required=True
    )

    test_pickle_parser.add_argument(
        '--c-pickle',
        type=argparse.FileType('rb'),
        help='Classifier input pickle file path'
    )

    dumps_parser = parser.add_argument_group('data dumping')

    dumps_parser.add_argument(
        '--histograms',
        type=argparse.FileType('w'),
        help='histograms dump JSON file path'
    )

    dumps_parser.add_argument(
        '--c-histograms',
        type=argparse.FileType('w'),
        help='combined histograms dump JSON file path'
    )

    dumps_parser.add_argument(
        '--h-stats',
        type=argparse.FileType('w'),
        help='histograms stats (class similarity) dump JSON file path'
    )

    dumps_parser.add_argument(
        '--c-stats',
        type=argparse.FileType('w'),
        help='classifier stats dump JSON file path'
    )


def add_subparsers(subparsers):
    learning_parser = subparsers.add_parser('learn', help='runs HTM in learning mode')
    add_learning_args(learning_parser)

    testing_parser = subparsers.add_parser('test', help='runs HTM in testing mode')
    add_testing_args(testing_parser)


def parse_args(args):
    parser = argparse.ArgumentParser(description="HTM learning & testing utility")

    subparsers = parser.add_subparsers(dest='mode')
    add_subparsers(subparsers)

    return parser.parse_args(args)


def get_file_name(args, name):
    if hasattr(args, name):
        f = getattr(args, name)
        if f is not None:
            f.close()
            return f.name
    return None


def get_attr(args, name):
    if hasattr(args, name):
        return getattr(args, name)
    return None


def main(args):
    log_level = getattr(logging, args.log.upper(), None)
    logging.getLogger().setLevel(log_level)

    cls = ModularCLI if args.modular else SingleCLI

    cli = cls(
        get_file_name(args, 'config'),
        args.visualizations,
        args.progress
    )

    if args.mode == 'learn':
        cli.learn(
            get_attr(args, 'iterations'),
            get_attr(args, 'reporter'),
            get_attr(args, 'partial_stats'),
            get_file_name(args, 'i_pickle'),
            get_file_name(args, 'o_pickle'),
            get_file_name(args, 'c_pickle'),
            get_file_name(args, 'c_stats')
        )
    elif args.mode == 'test':
        cli.test(
            get_file_name(args, 'i_pickle'),
            get_file_name(args, 'c_pickle'),
            get_file_name(args, 'histograms'),
            get_file_name(args, 'c_histograms'),
            get_file_name(args, 'h_stats'),
            get_file_name(args, 'c_stats'),
            get_attr(args, 'testing_dir')
        )
    else:
        cli.work()


def run(args):
    if args.profile:
        config = Config(max_depth=1)
        graphviz = GraphvizOutput(output_file='pycallgraph-%s.png' % (datetime.datetime.now().isoformat(), ))
        graphviz.set_config(config)
        with PyCallGraph(output=graphviz):
            main(args)
    else:
        main(args)


if __name__ == "__main__":
    run(parse_args(sys.argv[1:]))
