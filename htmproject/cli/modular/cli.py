import copy
import logging
import operator
import os
import sys

import numpy

from htmproject.classifiers.sklearn import SVMClassifier
from htmproject.interfaces.cli import CLI, HTMWrapper, Pickler, Dumper
from htmproject.interfaces.cli.exceptions import InputError, TooManyIterationsError, PickleError
from htmproject.mappers.filenames import FileNamesMapper
from htmproject.visualizers.plot import PlotVisualizer


class ModularCLI(CLI):
    def __init__(self, *args, **kwargs):
        self.__subdirs = {}

        super(ModularCLI, self).__init__(*args, **kwargs)

        self._logger.info('CLI ENGINE: MODULAR')

    def __get_subdirs(self, path):
        if path not in self.__subdirs:
            self.__subdirs[path] = [
                name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))
            ]
        return self.__subdirs[path]

    def _setup_learning(self, *args, **kwargs):
        super(ModularCLI, self)._setup_learning(*args, **kwargs)

        self.__setup_htm_modules()

    def _setup_testing(self, *args, **kwargs):
        super(ModularCLI, self)._setup_testing(*args, **kwargs)

        self.__setup_htm_modules()

    def __setup_htm_modules(self):
        self.__htm_modules = None
        self.__load_htm_modules()
        if self.__htm_modules is None:
            self.__create_htm_modules()

    def __create_htm_modules(self):
        self.__htm_modules = {}
        subdirs = self.__get_subdirs(self._learning_dir)
        if len(subdirs):
            for class_name in subdirs:
                self.__htm_modules[class_name] = HTMWrapper(
                    config=copy.deepcopy(self._config),
                    class_name=class_name,
                    enable_stats=self._enable_stats,
                    partial_stats_frequency=self._partial_stats_frequency,
                    enable_reporter=self._enable_reporter,
                    enable_progress=self._enable_progress
                )
        else:
            raise InputError('No subdirs found in learning set directory.')

    def _start_learning(self):
        need_training = True
        skip_testing = False
        classifier = None
        learning_stats = None
        testing_stats = None
        modules_to_train = self.__htm_modules.keys()

        while need_training and len(modules_to_train):
            modules_to_train = self.__train_modules(modules_to_train)
            if not len(modules_to_train):
                self._logger.warning('All HTM modules reached maximum learning iterations amount.')

            skip_testing, learning_stats, classifier = self.__train_classifier()
            if not skip_testing:
                need_training, testing_stats = self.__test_modules(classifier)

        # run test even after unsuccessful training
        if skip_testing:
            _, testing_stats = self.__test_modules(classifier)

        self._dump_classifier_stats(learning_stats, testing_stats)
        self._save_state(classifier)

    def _start_testing(self):
        if self._classifier_pickle_path is not None:
            self._logger.info('Loading classifier...')
            classifier = Pickler.load(self._classifier_pickle_path)
            self._logger.info('Classifier loaded')

            _, classifier_stats = self.__test_modules(classifier, True)
            self._dump_classifier_stats(testing_stats=classifier_stats)
        else:
            self.__get_combined_results(self._testing_dir, 'testing', True)

    def __get_testing_results(self, testing_dir, set_name):
        self._logger.info('Gathering testing results for %s set...' % (set_name, ))

        results = {}
        stats = {}

        for name, wrapper in self.__htm_modules.iteritems():
            test_results = wrapper.test(
                testing_dir,
                self._class_similarity_threshold,
                self._inter_class_similarity_threshold,
                visualizers=(PlotVisualizer,)
            )

            stats[name] = test_results['stats']

            for chunk_name, histogram in test_results['histograms'].iteritems():
                if chunk_name not in results:
                    results[chunk_name] = {
                        'class_name': FileNamesMapper.extract_name(chunk_name),
                        'histograms': {}
                    }
                results[chunk_name]['histograms'][name] = histogram

        self._logger.info('Testing results gathered')

        return results, stats

    def __combine_results(self, testing_results):
        self._logger.info('Combining results...')

        keys = testing_results.values()[0]['histograms'].keys()
        keys.sort()

        combined_results = {}
        for chunk_name, item in testing_results.iteritems():
            combined_results[chunk_name] = {
                'class_name': item['class_name'],
                'histogram': self.__combine_histograms(item['histograms'], keys)
            }

        self._logger.info('Results combined')

        return combined_results

    @staticmethod
    def __combine_histograms(histograms, keys):
        combined_histogram = numpy.array([], dtype=numpy.float64)
        for key in keys:
            combined_histogram = numpy.hstack((combined_histogram, histograms[key]))
        return combined_histogram

    def _start_working(self):
        pass

    def __train_modules(self, modules_to_train):
        next_to_train = copy.copy(modules_to_train)
        for name in modules_to_train:
            wrapper = self.__htm_modules[name]
            try:
                wrapper.learn(
                    os.path.join(self._learning_dir, name),
                    self._testing_dir,
                    self._max_learning_iterations,
                    self._class_similarity_threshold,
                    self._inter_class_similarity_threshold,
                    visualizers=(PlotVisualizer, )
                )
            except TooManyIterationsError:
                next_to_train.remove(name)
        return next_to_train

    def __train_classifier(self):
        classifier = SVMClassifier()

        results = self.__get_results_for_classifier(self._learning_dir, 'learning')

        self._logger.info('Training classifier...')
        classifier.train(results['histograms'], results['mapped_classes'])
        self._logger.info('Classifier trained')

        stats = self._get_classifier_stats(classifier, results['histograms'], results['mapped_classes'],
                                           results['class_names'], 'learning')

        if stats['f1_score'] < self._f1_score_threshold:
            return True, stats, classifier

        return False, stats, classifier

    def __get_results_for_classifier(self, testing_dir, set_name, with_dump=False):
        combined_results = self.__get_combined_results(testing_dir, set_name, with_dump)

        classes = list(set(map(operator.itemgetter('class_name'), combined_results.itervalues())))
        mapper = FileNamesMapper(classes)
        mapped_classes = mapper.map(combined_results.keys())

        histograms = map(operator.itemgetter('histogram'), combined_results.itervalues())

        return {
            'mapped_classes': mapped_classes,
            'histograms': histograms,
            'class_names': mapper.get_names()
        }

    def __get_combined_results(self, testing_dir, set_name, with_dump=False):
        testing_results, testing_stats = self.__get_testing_results(testing_dir, set_name)
        combined_results = self.__combine_results(testing_results)
        if with_dump:
            self.__dump_histograms(combined_results, testing_results, testing_stats)
        return combined_results

    def __test_modules(self, classifier, with_dump=False):
        """
        Test HTM modules using testing set

        :param Classifier classifier: Trained classifier
        :rtype: bool
        :return: Should HTM modules be trained further?
        """
        results = self.__get_results_for_classifier(self._testing_dir, 'testing', with_dump)

        stats = self._get_classifier_stats(classifier, results['histograms'], results['mapped_classes'],
                                           results['class_names'], 'testing')

        if stats['f1_score'] < self._f1_score_threshold:
            return True, stats

        return False, stats

    def __log_class_done(self):
        if self._enable_progress and 0 < self._logger.getEffectiveLevel() < logging.WARNING:
            sys.stdout.write('\n')
            sys.stdout.flush()

    def __log_iterations_done(self, elapsed_iterations):
        self._logger.info('Elapsed iterations: %i' % (elapsed_iterations,))

    def _save_state(self, classifier):
        self.__save_htm_modules()
        self._save_classifier(classifier)

    def __load_htm_modules(self):
        if self._input_pickle_path is not None:
            self._logger.info('Loading trained HTM modules...')
            try:
                self.__htm_modules = Pickler.load(self._input_pickle_path)
                for wrapper in self.__htm_modules.values():
                    wrapper.toggle_stats(self._enable_stats)
                    wrapper.toggle_reporter(self._enable_reporter)
                    wrapper.toggle_progress(self._enable_progress)
                    wrapper.set_partial_stats_frequency(self._partial_stats_frequency)
                    wrapper.update_config(copy.deepcopy(self._config))
                self._logger.info('HTM modules loaded')
            except EOFError:
                self._logger.critical('Empty or corrupted pickle file. Aborting.')
                raise PickleError()

    def __save_htm_modules(self):
        if self._output_pickle_path is not None:
            self._logger.info('Saving trained HTM modules...')
            Pickler.dump(self._output_pickle_path, self.__htm_modules)
            self._logger.info('HTM modules saved')

    def __dump_histograms(self, combined_results, testing_results, testing_stats):
        dumper = Dumper()

        dumper.dump(
            self._histograms_path,
            testing_results
        )
        dumper.dump(
            self._histograms_stats_path,
            testing_stats
        )
        dumper.dump(
            self._combined_histograms_path,
            combined_results
        )
