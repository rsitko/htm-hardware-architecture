import numpy
from .extractor import Extractor
from .column_extractor import ColumnExtractor
from .sp_state_extractor import SPStateExtractor


class LayerExtractor(Extractor):
    @staticmethod
    def extract(layer, input_item):
        column_extractor = ColumnExtractor()
        sp_state_extractor = SPStateExtractor()

        columns = layer.get_columns()
        active_columns = layer.get_current_active_columns()

        data = {
            'columns': [column_extractor.extract(column, input_item) for column in columns],
            'total_columns_count': len(columns),
            'active_columns_count': len(active_columns),
            'active_columns_percent': (len(active_columns)*100.0)/len(columns),
            'is_learning_enabled': layer.is_learning_enabled(),
            'layer_index': layer.get_layer_index(),
            'sp': sp_state_extractor.extract(layer.get_sp_state())
        }

        data['columns_transposed'] = [
            {'cells': c}
            for c in numpy.array(
                [ce['cells'] for ce in data['columns']]
            ).transpose().tolist()
        ]

        return data