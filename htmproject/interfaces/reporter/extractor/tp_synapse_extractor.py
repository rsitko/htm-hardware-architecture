from .extractor import Extractor


class TPSynapseExtractor(Extractor):
    @staticmethod
    def extract(synapse):
        cell = synapse.get_attached_cell()

        return {
            'is_active': synapse.is_active(),
            'is_learning': synapse.is_learning(),
            'is_connected': synapse.is_connected(),
            'perm_value': synapse.get_perm_value(),
            'synapse_index': synapse.get_synapse_index(),
            'segment_index': synapse.get_segment_index(),
            'cell_index': cell.get_cell_index(),
            'column_index': cell.get_column_index()
        }