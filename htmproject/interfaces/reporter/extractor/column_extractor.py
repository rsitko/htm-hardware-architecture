from .extractor import Extractor
from .cell_extractor import CellExtractor
from .column_connector_extractor import ColumnConnectorExtractor


class ColumnExtractor(Extractor):
    @staticmethod
    def extract(column, input_item):
        cell_extractor = CellExtractor()
        connector_extractor = ColumnConnectorExtractor()

        return {
            'cells': [cell_extractor.extract(cell) for cell in column.get_cells()],
            'column_index': column.get_column_index(),
            'is_active': column.is_active(),
            'connector': connector_extractor.extract(column.get_connector(), input_item)
        }