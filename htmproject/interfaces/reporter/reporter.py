class Reporter(object):
    """
    Abstract reporter class, loads config & defines interface
    """

    def __init__(self, htm, config):
        """

        :param htm: HTM instance
        :param config: Configuration object
        :return:
        """
        self.htm = htm
        self.iteration_index = 0
        self.config = config

    def start(self):
        """
        Prepare for logging
        :return:
        """
        raise NotImplementedError()

    def stop(self):
        """
        Finish logging
        :return:
        """
        raise NotImplementedError()

    def log_iteration(self, input_item, output_item):
        """
        Log single iteration
        :param input_item:
        :param output_item:
        :return:
        """
        raise NotImplementedError()

