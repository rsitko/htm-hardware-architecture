class Mapper(object):
    def __init__(self, names):
        self._names = names

    def get_names(self):
        return self._names

    def map(self, list_to_map):
        return [
            self._names.index(v) for v in list_to_map
        ]
