class Writer(object):
    """
    Abstract writer class
    """

    def save_data_item(self, item):
        """
        :param item: numpy.ndarray
        :return: save operation status
        :rtype: bool
        """
        raise NotImplementedError()

    def open(self):
        raise NotImplementedError()

    def close(self):
        raise NotImplementedError()
