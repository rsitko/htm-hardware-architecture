import copy
import logging
import numpy
import operator
import sys

from htmproject.analyzers.multiple_classes import MultipleClassesAnalyzer
from htmproject.analyzers.single_class import SingleClassAnalyzer
from htmproject.core import HTM, Iterator
from htmproject.interfaces.cli.exceptions import ConfigError, TooManyIterationsError
from htmproject.iostreamers.null import NullWriter
from htmproject.iostreamers.video_file import VideoFileChunkReader
from htmproject.encoders.adaptive_video import AdaptiveVideoEncoder
from htmproject.mappers.filenames import FileNamesMapper
from htmproject.reporters.html import HTMLReporter
from htmproject.visualizers.composite import CompositeVisualizer


class HTMWrapper(object):
    def __init__(
            self,
            config,
            class_name='default',
            enable_stats=True,
            partial_stats_frequency=1000,
            enable_reporter=False,
            enable_progress=True,
            htm=None,
            single_mode=False
    ):
        self.__logger = logging.getLogger(class_name if class_name else None)

        self.__config = config
        self.__class_name = class_name

        self.__single_mode = single_mode

        self.__enable_stats = enable_stats
        self.__partial_stats_frequency = partial_stats_frequency
        self.__enable_reporter = enable_reporter
        self.__enable_progress = enable_progress

        self.__whole_learning_set_seen = False
        self.__learning_iterations_offset = 0
        self.__learn_set_up = False

        self.__setup_htm(htm)
        self.reset_iteration_char()

        self.__learning_analyzer = None
        self.__testing_analyzer = None

    def toggle_stats(self, state):
        self.__enable_stats = state

    def toggle_reporter(self, state):
        self.__enable_reporter = state

    def toggle_progress(self, state):
        self.__enable_progress = state

    def set_partial_stats_frequency(self, amount):
        self.__partial_stats_frequency = amount

    def update_config(self, config):
        # those settings cannot be changed
        core = self.__config['core']
        encoder = self.__config['encoder']

        self.__config = config
        self.__config['core'] = core
        self.__config['encoder'] = encoder

    def __setup_htm(self, htm):
        encoder = AdaptiveVideoEncoder(self.__config, True)

        if htm is None:
            self.__logger.info('Creating \'%s\' HTM...' % (self.__class_name, ))
            try:
                self.__htm = HTM(self.__config, encoder.get_binary_space_size())
                self.__logger.info('HTM ready')
            except ValueError as e:
                self.__logger.critical('HTM could not be created. Reason:\n%s' % (e.message, ))
                exit(1)
        else:
            self.__htm = htm

        self.__iterator = Iterator(
            config=self.__config,
            encoder=encoder,
            htm=self.__htm
        )

    def learn(
            self,
            learning_dir,
            testing_dir,
            max_learning_iterations,
            class_similarity_threshold,
            inter_class_similarity_threshold,
            visualizers=None
    ):
        if not self.__learn_set_up:
            self.__setup_learning(
                learning_dir,
                class_similarity_threshold,
                max_learning_iterations,
                visualizers
            )
            self.__learn_set_up = True
        else:
            self.__whole_learning_set_seen = False

        self.__setup_testing(
            testing_dir,
            class_similarity_threshold,
            inter_class_similarity_threshold,
            visualizers
        )

        return self.__start_learning()

    def test(
            self,
            testing_dir,
            class_similarity_threshold,
            inter_class_similarity_threshold,
            visualizers=None
    ):
        self.__setup_testing(
            testing_dir,
            class_similarity_threshold,
            inter_class_similarity_threshold,
            visualizers
        )

        return self.__start_testing()

    def __run_chunk(self, chunk, analyzer, offset=0, report=False):
        i = 0
        try:
            for i in xrange(chunk['size']):
                self.__iterator.iterate()

                encoded_input = self.__iterator.get_encoded_input()
                encoded_output = self.__iterator.get_encoded_output()

                analyzer.save_iteration_stats(encoded_input, encoded_output)

                if report:
                    self.__reporter.log_iteration(encoded_input, encoded_output)

                self.__log_iteration(offset + i)
        except StopIteration:
            self.__logger.debug('\nError while reading frame %i (out of %i) from %s chunk.' % (
                i,
                chunk['size'],
                chunk['name']
            ))
            raise StopIteration()

        self.__switch_iteration_char()

        chunk_info = analyzer.save_chunk_info(chunk['name'], chunk['size'])
        analyzer.reset_chunk_outputs()

        return chunk_info

    def get_htm(self):
        return self.__htm

    def get_iterator(self):
        return self.__iterator

    def get_last_learning_stats(self):
        if self.__learning_analyzer is None:
            return None
        else:
            stats = {}
            stats.update(self.__learning_analyzer.get_last_class_similarity())
            return stats

    def get_last_testing_stats(self):
        if self.__testing_analyzer is None:
            return None
        else:
            stats = {}
            stats.update(self.__testing_analyzer.get_last_class_similarity())
            stats.update(self.__testing_analyzer.get_last_inter_class_similarity())
            return stats

    def __setup_learning(
            self,
            learning_dir,
            class_similarity_threshold,
            max_learning_iterations,
            visualizers
    ):
        learning_config = copy.deepcopy(self.__config)
        learning_config['iostreamer.video_frames_in_dir'] = learning_dir

        self.__learning_reader = VideoFileChunkReader(learning_config)
        self.__learning_data_set_size = self.__learning_reader.get_data_set_size()

        if max_learning_iterations:
            self.__max_learning_iterations = max_learning_iterations
        else:
            self.__max_learning_iterations = self.__learning_data_set_size
        self.__elapsed_learning_iterations = 0
        self.__whole_learning_set_seen = False

        self.__learning_analyzer = SingleClassAnalyzer(
            self.__htm,
            self.__learning_data_set_size,
            self.__config
        )

        self.__learning_visualizer = None
        if self.__enable_stats:
            self.__learning_visualizer = CompositeVisualizer(
                self.__learning_analyzer,
                self.__config,
                visualizers=visualizers,
                thresholds={
                    'class_similarity': {
                        'threshold': class_similarity_threshold,
                        'above': True
                    }
                }
            )

        if self.__learning_data_set_size > self.__max_learning_iterations:
            raise ConfigError('Learning set size (%i) is bigger than maximum allowed learning iterations (%i).' % (
                self.__learning_data_set_size,
                self.__max_learning_iterations
            ))

        if self.__enable_reporter:
            self.__reporter = HTMLReporter(self.__htm, self.__config)

    def __setup_testing(
            self,
            testing_dir,
            class_similarity_threshold,
            inter_class_similarity_threshold,
            visualizers
    ):
        testing_config = copy.deepcopy(self.__config)
        testing_config['iostreamer.video_frames_in_dir'] = testing_dir

        self.__testing_reader = VideoFileChunkReader(testing_config)
        self.__testing_data_set_size = self.__testing_reader.get_data_set_size()

        files_list = self.__testing_reader.get_files_list()
        mapper = FileNamesMapper(FileNamesMapper.names_from_list(files_list))

        self.__testing_analyzer = MultipleClassesAnalyzer(
            mapper,
            self.__htm,
            self.__testing_data_set_size,
            self.__config
        )

        self.__testing_visualizer = None
        if self.__enable_stats:
            self.__testing_visualizer = CompositeVisualizer(
                self.__testing_analyzer,
                self.__config,
                visualizers=visualizers,
                thresholds={
                    'class_similarity': {
                        'threshold': class_similarity_threshold,
                        'above': True
                    },
                    'inter_class_similarity': {
                        'threshold': inter_class_similarity_threshold,
                        'above': False
                    }
                }
            )

        self.__class_similarity_threshold = class_similarity_threshold
        self.__inter_class_similarity_threshold = inter_class_similarity_threshold

    def __start_learning(self):
        self.__logger.info('Starting \'%s\' HTM training...' % (self.__class_name, ))

        self.__learning_analyzer.start()

        if self.__enable_reporter:
            self.__reporter.start()

        too_many_iterations = False
        learn = True
        try:
            while learn:
                self.__learn()
                learn = self.__test()
        except TooManyIterationsError:
            self.__logger.warning('\'%s\' HTM reached maximum learning iterations amount' % (self.__class_name, ))
            too_many_iterations = True

        self.__learning_iterations_offset = self.__elapsed_learning_iterations

        self.__learning_analyzer.stop()

        if self.__enable_reporter:
            self.__reporter.stop()

        if too_many_iterations:
            raise TooManyIterationsError()
        else:
            self.__logger.info('Training for \'%s\' HTM finished' % (self.__class_name, ))
            return self.__get_testing_analyzer_results()

    def __start_testing(self):
        self.__test()

        return self.__get_testing_analyzer_results()

    def __get_testing_analyzer_results(self):
        results = {
            'histograms': {
                item['name']: item['histogram']
                for item in self.__testing_analyzer.get_chunk_info()
            },
            'stats': self.get_last_testing_stats()
        }

        return results

    def __learn(self):
        """
        HTM learning mode
        """
        if not self.__max_learning_iterations - self.__elapsed_learning_iterations:
            raise TooManyIterationsError()

        self.__logger.info('\'%s\' HTM is entering learning mode...' % (self.__class_name, ))

        done = False
        offset = 0
        whole_set_iterated = False
        elapsed_iterations = 0
        partials_counter = 0

        self.reset_iteration_char()

        self.__htm.toggle_learning(True)

        self.__iterator.set_reader(self.__learning_reader)
        self.__iterator.set_writer(NullWriter())
        self.__iterator.start()

        chunk = self.__learning_reader.next_chunk()
        while not done and self.__max_learning_iterations - self.__elapsed_learning_iterations >= chunk['size']:
            self.__run_chunk(chunk, self.__learning_analyzer, offset, self.__enable_reporter)

            self.__elapsed_learning_iterations += chunk['size']
            offset += chunk['size']

            chunk = self.__learning_reader.next_chunk()
            if chunk is None:
                self.__whole_learning_set_seen = True
                whole_set_iterated = True
                self.__learning_reader.reset()
                chunk = self.__learning_reader.next_chunk()

            if self.__whole_learning_set_seen:
                self.__learning_analyzer.save_class_stats()

                if whole_set_iterated:
                    done = self.__has_htm_learned()

            elapsed_iterations = self.__iterator.get_elapsed_iterations()
            if self.__enable_stats and self.__partial_stats_frequency and (
                elapsed_iterations / self.__partial_stats_frequency != partials_counter
            ):
                partials_counter = elapsed_iterations / self.__partial_stats_frequency
                self.__log_iterations_done(elapsed_iterations)
                self.__learning_visualizer.visualize_complete(
                    '%s-learn-%i-%i' % (
                        self.__class_name,
                        self.__learning_iterations_offset,
                        self.__elapsed_learning_iterations - 1
                    ),
                    self.__learning_iterations_offset
                )

        if elapsed_iterations and (not self.__partial_stats_frequency or (
            elapsed_iterations % self.__partial_stats_frequency != 0
        )):
            self.__log_iterations_done(elapsed_iterations)

            if self.__enable_stats:
                self.__learning_visualizer.visualize_complete(
                    '%s-learn-%i-%i' % (
                        self.__class_name,
                        self.__learning_iterations_offset,
                        self.__elapsed_learning_iterations - 1
                    ),
                    self.__learning_iterations_offset
                )

        self.__logger.info('Total elapsed learning iterations: %i' % (self.__elapsed_learning_iterations, ))
        self.__iterator.stop()

        self.__logger.debug('Profile info: %s' % (self.__htm.get_sp().get_profile_info(),))

        self.__logger.info('Learning mode left')

        if not done:
            raise TooManyIterationsError()

    def __test(self):
        """
        :rtype: bool
        :return: Does HTM still need learning?
        """
        self.__logger.info('\'%s\' HTM is entering testing mode...' % (self.__class_name, ))

        needs_learning = False

        self.reset_iteration_char()

        self.__htm.toggle_learning(False)

        self.__iterator.set_reader(self.__testing_reader)
        self.__iterator.set_writer(NullWriter())
        self.__iterator.start()

        self.__testing_analyzer.start()

        chunk = self.__testing_reader.next_chunk()
        offset = 0

        while chunk is not None:
            self.__run_chunk(chunk, self.__testing_analyzer, offset)

            offset += chunk['size']
            chunk = self.__testing_reader.next_chunk()

        self.__testing_analyzer.save_class_stats()

        elapsed_iterations = self.__iterator.get_elapsed_iterations()
        self.__log_iterations_done(elapsed_iterations)
        self.__iterator.stop()

        # check cosine class similarity for this HTM in testing set
        similarity = self.__testing_analyzer.get_last_class_similarity()

        if self.__single_mode:
            classes_list = similarity['class_similarity'].keys()
        else:
            classes_list = [self.__class_name]

        used_classes = []

        for class_name in classes_list:
            used_classes.append(class_name)

            sim = similarity['class_similarity'][class_name]['cosine']
            if sim < self.__class_similarity_threshold:
                needs_learning = True
                self.__logger.debug(
                    'Testing set cosine class similarity for \'%s\' is below threshold: %f' % (class_name, sim)
                )
            else:
                self.__logger.debug(
                    'Testing set cosine class similarity for \'%s\': %f' % (class_name, sim)
                )

            junk_similarity = self.__testing_analyzer.get_last_inter_class_similarity()
            for name, item in junk_similarity['inter_class_similarity'][class_name].items():
                if name in used_classes:
                    continue
                if item['cosine'] >= self.__inter_class_similarity_threshold:
                    needs_learning = True
                    self.__logger.debug(
                        'Testing set cosine \'%s\' - \'%s\' similarity is above threshold: %f' % (
                            class_name,
                            name,
                            item['cosine']
                        )
                    )
                else:
                    self.__logger.debug(
                        'Testing set cosine \'%s\' - \'%s\' similarity: %f' % (
                            class_name,
                            name,
                            item['cosine']
                        )
                    )

        self.__testing_analyzer.stop()

        if elapsed_iterations and self.__enable_stats:
            self.__testing_visualizer.visualize_complete(
                '%s-test' % (self.__class_name, )
            )

        self.__logger.debug('Profile info: %s' % (self.__htm.get_sp().get_profile_info(),))

        self.__logger.info('Testing mode left')

        return needs_learning

    def __log_iteration(self, i):
        if self.__enable_progress and 0 < self.__logger.getEffectiveLevel() < logging.WARNING:
            if i and i % 100 == 0:
                sys.stdout.write('\n')
            sys.stdout.write(self.__iteration_char)
            sys.stdout.flush()

    def __log_iterations_done(self, elapsed_iterations):
        if self.__enable_progress and 0 < self.__logger.getEffectiveLevel() < logging.WARNING:
            sys.stdout.write('\n')
            sys.stdout.flush()

        self.__logger.info('Elapsed iterations: %i' % (elapsed_iterations,))

    def reset_iteration_char(self):
        self.__iteration_char = '.'

    def __switch_iteration_char(self):
        self.__iteration_char = 'x' if self.__iteration_char == '.' else '.'

    def __has_htm_learned(self):
        """
        :rtype: bool
        :return:
        """

        similarity = self.__learning_analyzer.get_last_class_similarity()

        # checks if class cosine similarity is above threshold
        if (numpy.array(map(
                operator.itemgetter('cosine'),
                similarity['class_similarity'].values()
        )) < self.__class_similarity_threshold).any():
            return False

        # ok, we're all good
        return True

    def __getstate__(self):
        state = {
            'config': self.__dict__['_%s__config' % (self.__class__.__name__, )],
            'class_name': self.__dict__['_%s__class_name' % (self.__class__.__name__, )],
            'htm': self.__dict__['_%s__htm' % (self.__class__.__name__, )],
            'single_mode': self.__dict__['_%s__single_mode' % (self.__class__.__name__, )]
        }
        return state

    def __setstate__(self, ndict):
        self.__init__(
            ndict['config'],
            ndict['class_name'],
            htm=ndict['htm'],
            single_mode=ndict['single_mode']
        )
