params = {
    "learning_dir": "learning_files",
    "testing_dir": "testing_files",
    "working_dir": "working_files",

    "f1_score_threshold": 0.75,

    "class_similarity_threshold": 0.75,
    "inter_class_similarity_threshold": 0.5
}
