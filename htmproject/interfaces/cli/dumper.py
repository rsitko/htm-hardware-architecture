import json
import logging
import numpy


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return list(obj)
        return json.JSONEncoder(self, obj)


class Dumper(object):
    def __init__(self):
        self.__logger = logging.getLogger()

    def dump(self, filename, obj):
        self.__logger.info('Dumping data to %s file...' % (filename, ))

        with open(filename, 'w') as dump_file:
            json.dump(obj, dump_file, cls=Encoder)

        self.__logger.info('Data dumped')
