import logging
from pickler import Pickler
from dumper import Dumper

from htmproject.configs.json import JSONConfig
from htmproject.interfaces.cli.cli_params import params as cli_params


class CLI(object):
    def __init__(
            self,
            config_path='config.json',
            enable_stats=True,
            show_progress=True
    ):
        """
        Reads configuration from external file and configures execution according to its contents
        and constructor parameters

        :param str [config_path]: HTM Configuration file name
        :param bool [enable_stats]: Is statistics generation enabled?
        :param bool [show_progress]: Should progress marks be printed?
        """
        self._logger = logging.getLogger()

        self._config = JSONConfig(config_path)
        self._config.add_default_params_section('cli', cli_params)

        self._enable_reporter = False
        self._partial_stats_frequency = 0

        self._enable_progress = show_progress

        self._enable_stats = enable_stats
        self._logger.debug('Statistics visualizations enabled: %s' % (self._enable_stats,))

    def __start(self, mode_handler):
        """
        Starts HTM life cycle
        """

        self._logger.info('HTM life cycle starting...')
        mode_handler()
        self._logger.info('HTM life cycle done')

    def learn(
            self,
            max_iterations=2 ** 31,
            enable_reporter=False,
            partial_stats_frequency=1000,
            input_pickle_path=None,
            output_pickle_path=None,
            classifier_pickle_path=None,
            classifier_stats_path=None
    ):
        self._logger.info('MODE: LEARNING')

        self._enable_reporter = enable_reporter
        self._logger.debug('HTML iterations reporter enabled: %s' % (self._enable_reporter,))

        self._partial_stats_frequency = partial_stats_frequency
        self._logger.debug('Partial stats frequency: %i' % (self._partial_stats_frequency,))

        self._input_pickle_path = input_pickle_path
        self._output_pickle_path = output_pickle_path
        self._classifier_pickle_path = classifier_pickle_path

        if self._input_pickle_path is not None:
            self._logger.debug('HTM module(s) input pickle: %s' % (self._input_pickle_path,))
        if self._output_pickle_path is not None:
            self._logger.debug('HTM module(s) output pickle: %s' % (self._output_pickle_path,))
        if self._classifier_pickle_path is not None:
            self._logger.debug('Classifier pickle: %s' % (self._classifier_pickle_path,))

        self._classifier_stats_path = classifier_stats_path if classifier_stats_path is not None else 'classifier_stats.json'
        self._logger.debug('Classifier stats file path: %s' % (self._classifier_stats_path, ))

        self._setup_learning(max_iterations)
        self.__start(self._start_learning)

    def test(
            self,
            input_pickle_path=None,
            classifier_pickle_path=None,
            histograms_path=None,
            combined_histograms_path=None,
            histograms_stats_path=None,
            classifier_stats_path=None,
            testing_dir=None
    ):
        self._logger.info('MODE: TESTING')

        self._input_pickle_path = input_pickle_path
        self._classifier_pickle_path = classifier_pickle_path

        if self._input_pickle_path is not None:
            self._logger.debug('HTM module(s) input pickle: %s' % (self._input_pickle_path,))
        if self._classifier_pickle_path is not None:
            self._logger.debug('Classifier pickle: %s' % (self._classifier_pickle_path,))

        self._classifier_stats_path = classifier_stats_path if classifier_stats_path is not None else 'classifier_stats.json'
        self._logger.debug('Classifier stats file path: %s' % (self._classifier_stats_path, ))

        self._histograms_path = histograms_path if histograms_path is not None else 'histograms.json'
        self._combined_histograms_path = combined_histograms_path if combined_histograms_path is not None else 'combined_histograms.json'
        self._histograms_stats_path = histograms_stats_path if histograms_stats_path is not None else 'histograms_stats.json'

        self._logger.debug('Histograms dump file path: %s' % (self._histograms_path,))
        self._logger.debug('Combined histograms dump file path: %s' % (self._combined_histograms_path,))
        self._logger.debug('Histograms stats file path: %s' % (self._histograms_stats_path, ))

        self._setup_testing(testing_dir)
        self.__start(self._start_testing)

    def work(self):
        self._logger.info('MODE: WORKING')

        self._setup_working()

    def _setup_learning(self, max_iterations):
        self._get_learning_params(max_iterations)
        self._get_testing_params()

    def _setup_testing(self, testing_dir=None):
        self._get_testing_params(testing_dir)

    def _setup_working(self):
        self._get_working_params()

    def _start_learning(self):
        """
        Learning mode flow
        """
        raise NotImplementedError()

    def _start_testing(self):
        """
        Testing mode flow
        """
        raise NotImplementedError()

    def _start_working(self):
        """
        Working mode flow
        """
        raise NotImplementedError()

    def _get_learning_params(self, max_iterations):
        self._learning_dir = self._config.get('cli.learning_dir')
        self._logger.debug('Learning set dir: %s' % (self._learning_dir,))

        self._max_learning_iterations = max_iterations
        self._logger.debug('Maximum learning iterations: %i' % (self._max_learning_iterations,))

    def _get_testing_params(self, testing_dir=None):
        self._testing_dir = testing_dir if testing_dir is not None else self._config.get('cli.testing_dir')
        self._logger.debug('Testing set dir: %s' % (self._testing_dir,))

        self._f1_score_threshold = self._config.get('cli.f1_score_threshold')
        self._logger.debug('F1-score threshold: %f' % (self._f1_score_threshold,))

        self._class_similarity_threshold = self._config.get('cli.class_similarity_threshold')
        self._logger.debug('Class similarity threshold: %f' % (self._class_similarity_threshold,))

        self._inter_class_similarity_threshold = self._config.get('cli.inter_class_similarity_threshold')
        self._logger.debug('Inter-class similarity threshold: %f' % (self._inter_class_similarity_threshold,))

    def _get_working_params(self):
        self._working_dir = self._config.get('cli.working_dir')
        self._logger.debug('Working set dir: %s' % (self._working_dir,))

    def _save_classifier(self, classifier):
        if self._classifier_pickle_path is not None:
            self._logger.info('Saving trained classifier...')
            Pickler.dump(self._classifier_pickle_path, classifier)
            self._logger.info('Classifier saved')

    def _dump_classifier_stats(self, learning_stats=None, testing_stats=None):
        dumper = Dumper()

        dumper.dump(
            self._classifier_stats_path, {
                'learning': learning_stats,
                'testing': testing_stats
            }
        )

    def _get_classifier_stats(self, classifier, histograms, mapped_classes, class_names, set_name):
        self._logger.info('Classifying %s set...' % (set_name,))

        stats = classifier.get_classification_stats(
            histograms,
            mapped_classes,
            class_names
        )

        self._logger.info('Classification done')

        self._logger.debug('F1-score: %f' % (stats['f1_score'],))
        self._logger.debug('Confusion matrix:\n%s' % (stats['confusion_matrix'],))

        return stats
