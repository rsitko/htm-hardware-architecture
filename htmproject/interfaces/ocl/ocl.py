import pyopencl as ocl
import os
import sys
import inspect

from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException

KERNELS_LOCATION = 'ocls/kernels'
PROFILE = False


class OCL(object):
    """
    Template class for openCL applications
    """
    def __init__(self, kernel_name):
        try:
            device = self.__get_device()
        except IndexError:
            raise NoDeviceOCLException()

        self._profile = PROFILE
        properties = None
        if self._profile:
            properties = ocl.command_queue_properties.PROFILING_ENABLE
            self._exec_time = 0
            self._dev_to_host_time = 0
            self._host_to_dev_time = -float('inf')
            self._exec_amount = 0

        self.ctx = ocl.Context([device])
        self.queue = ocl.CommandQueue(self.ctx, properties=properties)
        self.kernel_path = os.path.join(self.__get_kernels_dir(), kernel_name)
        self.__load_kernel()

    @staticmethod
    def __get_device():
        """
        Finds all the available devices and uses the best among them.
        :return:
        """
        devices = []
        best = [
            ocl.device_type.ACCELERATOR,
            ocl.device_type.GPU,
            ocl.device_type.CPU,
            ocl.device_type.DEFAULT,
            ocl.device_type.ALL
        ]

        if ocl.get_cl_header_version()[1] > 1:
            best.insert(0, ocl.device_type.CUSTOM)

        for platform in ocl.get_platforms():
            for device in platform.get_devices():
                devices.append((device, device.type))

        devices.sort(cmp=lambda x, y: cmp(best.index(x[1]), best.index(y[1])))

        return devices[0][0]

    def __get_kernels_dir(self):
        """
        Gets kernels dir location. All the kernels are located in a single dir pointed
        by KERNELS_LOCATION relative to the current file location.
        """
        if getattr(sys, 'frozen', False):
            path = os.path.abspath(sys.executable)
        else:
            path = inspect.getabsfile(self.__get_kernels_dir)

        current_path = os.path.dirname(os.path.realpath(path))
        relative_kernel_dir_loc = os.path.abspath(os.path.join(current_path, os.pardir))  # move to the parent dir
        relative_kernel_dir_loc = os.path.abspath(os.path.join(
            relative_kernel_dir_loc, os.pardir))  # move to the parent dir
        kernel_dir = os.path.abspath(os.path.join(
            relative_kernel_dir_loc, KERNELS_LOCATION))  # move to the dir with kernels

        return kernel_dir

    def __load_kernel(self, show_kernel=False):
        f = open(self.kernel_path + ".cl")

        fstr = f.read()
        if show_kernel:
            print(fstr)

        self.program = ocl.Program(self.ctx, fstr).build()

    def _get_dest_buf_out_vec(self, *args, **kwargs):
        raise NotImplementedError()

    def _execute_kernel(self, *args, **kwargs):
        raise NotImplementedError()

    def _post_execute_kernel(self):
        pass

    def execute_kernel(self, *args, **kwargs):
        exec_evt = self._execute_kernel(*args, **kwargs)

        dest_buf, out_vec = self._get_dest_buf_out_vec()

        read_evt = ocl.enqueue_read_buffer(self.queue, dest_buf, out_vec, wait_for=[exec_evt])
        read_evt.wait()

        self._post_execute_kernel()

        if self._profile:
            self._exec_amount += 1
            self._exec_time += exec_evt.profile.end - exec_evt.profile.start
            self._dev_to_host_time += read_evt.profile.end - read_evt.profile.start

        return out_vec

    def get_profile_info(self):
        return {
            "exec_amount": self._exec_amount,
            "exec_time": self._exec_time,
            "dev_to_host_time": self._dev_to_host_time,
            "host_to_dev_time": self._host_to_dev_time
        } if self._profile else None
