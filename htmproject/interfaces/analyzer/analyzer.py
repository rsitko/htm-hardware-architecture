class Analyzer(object):
    def __init__(self, config):
        self._config = config

    def start(self):
        """
        Tells __learning_analyzer to wipe out possible old state and start accumulating stats
        :return:
        """
        raise NotImplementedError()

    def stop(self):
        """
        Tells __learning_analyzer to stop accumulating data and make final calculations
        :return:
        """
        raise NotImplementedError()

    def reset_chunk_outputs(self):
        """
        Starts collecting output data. Wipes out previous history.
        """
        raise NotImplementedError()

    def save_chunk_info(self, chunk_name, size=None):
        raise NotImplementedError()

    def get_chunk_info(self):
        raise NotImplementedError()

    def save_iteration_stats(self, encoded_input, decoded_output):
        """
        Tells __learning_analyzer that iteration has passed an it needs to update its internal state
        :return:
        """
        raise NotImplementedError()

    def get_iteration_stats(self):
        """
        :rtype: dict
        :return: Current set of statistics.
        """
        raise NotImplementedError()

    def get_complete_stats(self):
        """
        :rtype: dict
        :return: Complete set of statistics
        """
        raise NotImplementedError()

    def get_chunk_names(self):
        raise NotImplementedError()

    def get_chunk_histograms(self):
        raise NotImplementedError()

    def get_chunk_sizes(self):
        raise NotImplementedError()

    def get_chunk_histograms_by_name(self):
        raise NotImplementedError()

    def get_last_chunk_histograms(self):
        raise NotImplementedError()

    def get_last_chunk_histograms_by_name(self):
        raise NotImplementedError()
