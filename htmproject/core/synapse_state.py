class SynapseState(object):
    def __init__(self,
                 config,
                 namespace
                 ):
        self.__perm_value = float(config.get('core.%s.init_perm' % (namespace,)))
        self.__perm_inc = float(config.get('core.%s.perm_inc' % (namespace,)))
        self.__perm_dec = float(config.get('core.%s.perm_dec' % (namespace,)))
        self.__connected_perm = float(config.get('core.%s.connected_perm' % (namespace,)))

    def get_perm_value(self):
        return self.__perm_value

    def set_perm_value(self, value):
        self.__perm_value = value

    def inc_perm_value(self):
        self.__perm_value = min(self.__perm_value + self.__perm_inc, 1)  # perm_value does not exceed 1

    def dec_perm_value(self):
        self.__perm_value = max(self.__perm_value - self.__perm_dec, 0)  # perm_value does not drop below 0

    def is_connected(self):
        return self.__perm_value >= self.__connected_perm

    def boost_perm_value(self):
        """
        It is used solely by SP to boost synapses' connections.
        Increment permValue by 10 % * connectedPerm
        """
        # TODO: decide between two options
        # increase perm value by a percent of max possible perm value (==1)
        # self.perm_value = min(self.perm_value + 0.1 * self.connected_perm, 1)
        # alternative: increase perm value by a percent of current value
        self.__perm_value = min(self.__perm_value * (1 + 0.1 * self.__connected_perm), 1)
