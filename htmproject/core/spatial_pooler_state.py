class SpatialPoolerState(object):

    def __init__(self, columns_amount):
        """

        :param int columns_amount:
        """
        self.__columns_amount = columns_amount
        self.__inhibition_radius = 3

    def get_inhibition_radius(self):
        """

        :return: inhibition radius
        :rtype: int
        """
        return self.__inhibition_radius

    def set_inhibition_radius(self, value):
        """

        :param int value:
        """
        self.__inhibition_radius = max(1, min(value, self.__columns_amount-1))
