from update import Update
from htmproject.core.segment import Segment


class CellUpdate(Update):
    """

    """
    def __init__(self, cell, config, make_segment_sequential=False):
        """

        :return:
        """
        super(CellUpdate, self).__init__()
        self.segment = Segment(cell, config, make_segment_sequential)

    def get_segment(self):
        """

        :return:
        """
        return self.segment
