from .synapse import Synapse
from .synapse_state import SynapseState


class TPSynapse(Synapse):
    """
    Synapse used in TP
    """

    def __init__(self, config, segment, attached_cell):
        super(TPSynapse, self).__init__(config, 'tp')
        self.__attached_cell = attached_cell
        self.__previous_state = SynapseState(self._config, 'tp')

        self.__segment = segment

    def is_active(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self.__attached_cell.is_active() and (ignore_connected_perm or self._current_state.is_connected())

    def was_active(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self.__attached_cell.was_active() and (ignore_connected_perm or self.__previous_state.is_connected())

    def get_attached_cell(self):
        """

        :return:
        """
        return self.__attached_cell

    def was_connected(self):
        """

        :return:
        """
        return self.__previous_state.is_connected()

    def shift_states(self):
        """

        """
        self.__previous_state = self._current_state
        self._current_state = SynapseState(self._config, 'tp')

    def is_learning(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self.__attached_cell.is_learning() and (ignore_connected_perm or self._current_state.is_connected())

    def was_learning(self, ignore_connected_perm=False):
        """

        :return:
        """
        return self.__attached_cell.was_learning() and (ignore_connected_perm or self.__previous_state.is_connected())

    # logging / debugging
    def get_synapse_index(self):
        return self.__segment.get_synapse_index(self)

    def get_segment_index(self):
        return self.__segment.get_segment_index()
