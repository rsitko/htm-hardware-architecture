import numpy


class TemporalPooler(object):
    """
    Class TemporalPooler
    """

    def __phase_zero(self, layer):
        """
        Looks through queued updates and removes the outdated ones.
        Outdated means older than two cycles.

        :param Layer layer:
        """
        for column in layer.get_columns():
            for cell in column.get_cells():
                cell.update_queue()

    def __phase_one(self, layer):
        """
        Calculates active and learning state for each cell in
        active columns.
        """
        active_columns = layer.get_current_active_columns()
        previous_learning_cells = layer.get_previous_learning_cells()

        for column in active_columns:
            predicted = False
            learning = False
            cells = column.get_cells()
            for cell in cells:
                if cell.was_predicted():
                    segment = cell.get_previous_active_segment()
                    if segment is not None and segment.is_sequential():
                        predicted = True
                        cell.set_active(True)
                        if layer.is_learning_enabled() and segment.was_learning():
                            learning = True
                            cell.set_learning(True)

            if not predicted:
                for cell in cells:
                    cell.set_active(True)

            if layer.is_learning_enabled() and not learning:
                best_cell = column.get_previous_best_matching_cell()
                best_segment = best_cell.get_previous_best_matching_segment()
                best_cell.set_learning(True)
                if best_segment is None:
                    best_segment = best_cell.queue_new_segment(True)
                best_segment.queue_previous_step_segment_updates(True, True, previous_learning_cells[:])

    def __phase_two(self, layer):
        """
        Propose future changes if the prediction is met.

        :param Layer layer:
        """
        current_learning_cells = layer.get_current_learning_cells()
        previous_learning_cells = layer.get_previous_learning_cells()

        for column in layer.get_columns():
            for cell in column.get_cells():
                for segment in cell.get_segments():
                    if segment.is_active():
                        cell.set_predictive(True)

                        if layer.is_learning_enabled():
                            segment.queue_current_step_segment_updates(
                                cells_in_learning_state=current_learning_cells
                            )
                            best_segment = cell.get_previous_best_matching_segment()
                            if best_segment is None:
                                best_segment = cell.queue_new_segment()
                            best_segment.queue_previous_step_segment_updates(
                                False,
                                True,
                                cells_in_learning_state=previous_learning_cells
                            )

    def __phase_three(self, layer):
        """
        Apply queued chances if requirements are met.

        :param Layer layer:
        """
        for column in layer.get_columns():
            for cell in column.get_cells():
                if cell.is_learning():
                    cell.apply_queued_changes(True)
                elif not cell.is_predicted() and cell.was_predicted():
                    cell.apply_queued_changes(False)

    def update_layer_state(self, layer):
        """
        Launches all the phases of the TemporalPooler algorithm.

        :param Layer layer:
        """
        self.__phase_zero(layer)
        self.__phase_one(layer)
        self.__phase_two(layer)
        if layer.is_learning_enabled():
            self.__phase_three(layer)

    def get_layer_output(self, layer):
        """
        Returns logic OR of active and predictive cells' states.

        :param Layer layer:
        :return: []
        """
        output = []

        for column in layer.get_columns():
            state = False
            for cell in column.get_cells():
                if cell.is_active() or cell.is_predicted():
                    state = True
            output.append(state)

        return numpy.array(output)


