import numpy

from htmproject.core.sp_synapse import SPSynapse


class ColumnConnector(object):
    """
    Holds synapses for each column.
    To be used with SP.
    It is assumed that the input size is equal to the number of columns.
    """
    def __init__(self, column, config, input_length, columns_amount):
        self.__column = column
        self.__config = config
        self.__input_length = input_length
        self.__columns_amount = columns_amount

        self.__overlap_history_buffer_length = int(self.__config.get('core.sp.overlap_history_buffer_length'))
        self.__active_history_buffer_length = int(self.__config.get('core.sp.active_history_buffer_length'))
        self.__boost = self.__config.get('core.sp.boost')
        self.__inhibition_radius = int(self.__config.get('core.sp.init_inhibition_radius'))
        self.__number_of_synapses_per_column_connector = int(self.__config.get('core.sp.synapses_per_column'))

        self.__active_after_inhibition = False
        self.__overlap = None  # None instead of 0, so it throws exception when trying to use uninitialized
        self.__max_duty_cycle = 0
        self.__min_duty_cycle = 0
        self.__overlap_duty_history = []
        self.__active_duty_history = []
        self.__overlap_duty_cycle = 0
        self.__active_duty_cycle = 0
        self.__synapses = []

        self.__generate_connections()
        self.__synapses_amount = len(self.__synapses)
        self.__min_overlap = int(self.__config.get('core.sp.min_overlap'))

    def __generate_connections(self):
        """
        Generates input connections. Synapses are connected to some of the input bits.
        The input may come from both encoder and TP output.
        n - length of input data
        """
        ones = numpy.ones(self.__number_of_synapses_per_column_connector, dtype=bool)
        zeros = numpy.zeros(self.__input_length - self.__number_of_synapses_per_column_connector, dtype=bool)

        input_indexes = numpy.hstack((ones, zeros))
        numpy.random.shuffle(input_indexes)

        for i in range(0, len(input_indexes)):
            if input_indexes[i]:
                self.__synapses.append(SPSynapse(self.__config, i))

    def get_boost(self):
        """
        :rtype: float
        :return: boost value
        """
        return self.__boost

    def update_boost(self):
        """
        Calculates and set new boost value for column
        """
        adc = self.get_active_duty_cycle()
        mdc = self.get_min_duty_cycle()
        if not adc or not mdc or adc > mdc:
            self.__boost = 1
        else:
            self.__boost = mdc / adc

    def calculate_overlap(self, input_item):
        self.__overlap = reduce(
            lambda x, y: x + int(y.is_active(input_item)),
            self.get_synapses(),
            0
        )

    def is_active_before_inhibition(self):
        """
        :rtype: bool
        :return: overlap > minOverlap
        """

        return self.__overlap > self.__min_overlap

    def get_boosted_overlap(self):
        """

        :return:
        """
        return self.__overlap * self.__boost if self.is_active_before_inhibition() else 0

    def set_active_after_inhibition(self):
        """
        Once the inhibition phase is done this is to be set if
        the column was chosen.
        """
        self.__active_after_inhibition = True

    def reset_active_after_inhibition(self):
        """
        Resets the flag after the current iteration is done.
        :return:
        """
        self.__active_after_inhibition = False

    def is_active_after_inhibition(self):
        """
        The column was selected as active during inhibition phase.
        :rtype: bool
        :return: If the column is active after inhibition phase.
        """
        return self.__active_after_inhibition

    def update_synapses(self, input_item):
        """
        Increments synapses which are connected to '1' and decrements the
        other ones.
        """
        for synapse in self.__synapses:
            if input_item[synapse.get_input_index()]:
                synapse.inc_perm_value()
            else:
                synapse.dec_perm_value()

    def update_active_duty_cycle(self):
        """

        :return:
        """
        # TODO: implement __active_duty_history as collections.deque or WrappedQueue
        # update the history buffer
        if len(self.__active_duty_history) < self.__active_history_buffer_length:
            self.__active_duty_history.append(self.is_active_after_inhibition())
        else:
            self.__active_duty_history.remove(self.__active_duty_history[0])
            self.__active_duty_history.append(self.is_active_after_inhibition())

        # compute activeDutyCycle
        self.__active_duty_cycle = sum(self.__active_duty_history)

    def get_active_duty_cycle(self):
        """
        ActiveDutyCycle - number of activation of a given column in the decided time
        range.

        :return:
        """
        return self.__active_duty_cycle

    def set_max_and_min_duty_cycle(self, max_duty_cycle):
        """

        :return:
        """
        self.__max_duty_cycle = max_duty_cycle
        self.__min_duty_cycle = 0.01 * self.__max_duty_cycle

    def get_min_duty_cycle(self):
        """
        :return:
        """
        return self.__min_duty_cycle

    def update_overlap_duty_cycle(self):
        """
        Updates the history buffer and computes overlapDutyCycle
        :return:
        """
        # TODO: implement __overlap_duty_history as collections.deque or WrappedQueue
        # update the history buffer
        if len(self.__overlap_duty_history) < self.__overlap_history_buffer_length:
            self.__overlap_duty_history.append(self.__overlap > self.__min_overlap)
        else:
            self.__overlap_duty_history.remove(self.__overlap_duty_history[0])
            self.__overlap_duty_history.append(self.__overlap > self.__min_overlap)

        # compute overlapDutyCycle
        self.__overlap_duty_cycle = sum(self.__overlap_duty_history)

    def get_overlap_duty_cycle(self):
        """

        :return:
        """
        return self.__overlap_duty_cycle

    def boost_perm_values(self):
        """
        Increment permValues of all the synapses if overlapDutyCycle < minDutyCycle
        by 10%*connectedPerm. It is used to equalize activity of all the connected synapses.
        """
        if self.__overlap_duty_cycle <= self.__min_duty_cycle:
            for synapse in self.__synapses:
                synapse.boost_perm_value()

    def update_inhibition_radius(self):
        """
        Computes new inhibition radius.
        """

        # find min value
        min_value = None
        for i in xrange(self.__synapses_amount):
            if self.__synapses[i].is_connected():
                min_value = i
                break

        # find max value
        max_value = None
        for i in xrange(self.__synapses_amount - 1, 0, -1):
            if self.__synapses[i].is_connected():
                max_value = i
                break

        if min_value is not None and max_value is not None:
            reception_field = max_value - min_value
            self.__inhibition_radius = max(int(round(
                (self.__columns_amount * reception_field) / (3 * self.__synapses_amount)
            )), 1)

    def get_inhibition_radius(self):
        """

        :rtype: int
        :return:
        """
        return self.__inhibition_radius

    def get_max_duty_cycle(self):
        return self.__max_duty_cycle

    def get_overlap(self):
        return self.__overlap

    def get_active_duty_history(self):
        return self.__active_duty_history

    def get_overlap_duty_history(self):
        return self.__overlap_duty_history

    def get_column(self):
        """

        :rtype: Column
        :return:
        """
        return self.__column

    def get_synapses(self):
        """

        :rtype: SPSynapse[]
        :return:
        """

        return self.__synapses

    def get_synapses_amount(self):
        """

        :return:
        """
        return self.__synapses_amount












