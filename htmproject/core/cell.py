from htmproject.core.cell_state import CellState
from htmproject.core.cell_update import CellUpdate


class Cell(object):
    """

    """

    def __init__(self, column, config):
        """

        """
        self.config = config
        self.cell_updates_queue = []
        self.current_state = CellState()
        self.previous_state = CellState()
        self.segments = []
        # for logging/debugging purposes
        self.__column = column

    def get_segments(self):
        """

        :rtype: Segment[]
        :return:
        """
        return self.segments

    def was_predicted(self):
        """

        :rtype: bool
        :return:
        """

        return self.previous_state.is_predictive()

    def is_predicted(self):
        """

        :rtype: bool
        :return:
        """
        return self.current_state.is_predictive()

    def set_predictive(self, state):
        """

        :param state:
        :return:
        """
        self.current_state.set_predictive(state)

    def was_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.previous_state.is_active()

    def is_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.current_state.is_active()

    def set_active(self, state):
        """

        :param state:
        :return:
        """
        self.current_state.set_active(state)

    def was_learning(self):
        """

        :rtype: bool
        :return:
        """
        return self.previous_state.is_learning()

    def is_learning(self):
        """

        :rtype: bool
        :return:
        """
        return self.current_state.is_learning()

    def set_learning(self, state):
        """

        :param state:
        :return:
        """

        self.current_state.set_learning(state)

    def get_previous_active_segment(self):
        """

        :return:
        """
        active_segments = []

        for segment in self.segments:
            if segment.was_active():
                active_segments.append(segment)

        active_segments.sort(key=lambda s: s.get_current_active_synapses_count())

        for segment in active_segments:
            if segment.is_sequential():
                return segment
            else:
                active_segments.remove(segment)

        return active_segments[0] if len(active_segments) else None

    def get_previous_best_matching_segment(self):
        """

        :return:
        """

        if len(self.segments):
            self.segments.sort(key=lambda s: s.get_current_active_synapses_count(True))
            return self.segments[0]

        return None

    def empty_queue(self):
        """

        """
        self.cell_updates_queue = []

    def queue_new_segment(self, make_segment_sequential = False):
        """

        :param make_segment_sequential:
        :rtype: Segment
        :return:
        """
        update = CellUpdate(self, self.config, make_segment_sequential)
        self.cell_updates_queue.append(update)
        return update.get_segment()

    def apply_queued_changes(self, reinforcement=True):
        """
        Increases or decreases permValues of synapses.
        If reinforcement is true active synapses have their permValues increased
        while all the other synapses have their values decreased.
        If reinforcement is false active synapses have their permValues decreased
        and all the other synapses remain the same.
        After permValues of the existing synapses are updated the queued new synapses
        are created with standard permValue.
        :param reinforcement: Boolean, True means positive reinforcement
        """

        for update in self.cell_updates_queue:
            self.segments.append(update.get_segment())

        for segment in self.segments:
            segment.apply_queued_changes(reinforcement)

        self.empty_queue()

    def update_queue(self):
        """
        Decrease TTL (Time To Live) in queued updates and removes updates for
        which TTL reaches zero.
        :return:
        """
        new_cell_updates_queue = []

        for update in self.cell_updates_queue:
            update.decrease_TTL()
            if update.get_TTL() > 0:
                new_cell_updates_queue.append(update)

        self.cell_updates_queue = new_cell_updates_queue

        for segment in self.segments:
            segment.update_queue()

    def clear_queued_updates(self):
        self.empty_queue()

        for segment in self.segments:
            segment.empty_queue()

    def shift_states(self):
        """

        """
        self.previous_state = self.current_state
        self.current_state = CellState()

        for segment in self.segments:
            segment.shift_states()

    # logging / debugging
    def get_cell_index(self):
        """

        :return:
        """
        return self.__column.get_cell_index(self)

    def get_column_index(self):
        """

        :return:
        """
        return self.__column.get_column_index()

    def get_segment_index(self, segment):
        """

        :param segment:
        :return:
        """
        return self.segments.index(segment)

    def get_segments_amount(self):
        """
        Returns a number of segments in the cell.
        :return int: number of segments in the cell.
        """
        return len(self.segments)

