from .synapse_state import SynapseState


class Synapse(object):
    """

    """

    def __init__(self, config, namespace):
        self._config = config
        self._current_state = SynapseState(self._config, namespace)

    def inc_perm_value(self):
        """

        :return:
        """
        self._current_state.inc_perm_value()

    def dec_perm_value(self):
        """

        :return:
        """
        self._current_state.dec_perm_value()

    def get_perm_value(self):
        return self._current_state.get_perm_value()

    def is_connected(self):
        return self._current_state.is_connected()
