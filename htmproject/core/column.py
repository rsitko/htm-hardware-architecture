from htmproject.core.cell import Cell
from htmproject.core.column_state import ColumnState
from htmproject.core.column_connector import ColumnConnector


class Column(object):
    """Class Column
    """
    def __init__(self, layer, config, input_length, cells_amount=8):
        """

        :param int cells_amount: amount of cells per column
        """
        self.previous_state = ColumnState()
        self.current_state = ColumnState()
        self.__connector = ColumnConnector(self, config, input_length, layer.get_columns_amount())
        self.__cells = []
        self.__cells_amount = cells_amount

        for i in range(0, cells_amount):
            self.__cells.append(Cell(self, config))

        # for logging / debugging purposes
        self.__layer = layer

    def set_active(self, state):
        """

        :param bool state:
        """
        self.current_state.set_active(state)

    def is_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.current_state.is_active()

    def was_active(self):
        """

        :rtype: bool
        :return:
        """
        return self.previous_state.is_active()

    def get_cells(self):
        """

        :rtype: Cell[]
        :return:
        """
        return self.__cells

    def get_previous_best_matching_cell(self):
        """

        :rtype: Cell
        :return:
        """

        tmp_segments = []

        for cell in self.get_cells():
            tmp_segments.append((cell, cell.get_previous_best_matching_segment()))

        tmp_segments.sort(key=lambda s: s[1].get_current_active_synapses_count(True) if s[1] is not None else 0)

        return tmp_segments[0][0]

    def get_current_learning_cell(self):
        """

        :rtype: Cell
        :return:
        """
        for cell in self.get_cells():
            if cell.is_learning():
                return cell

    def get_previous_learning_cell(self):
        """

        :rtype: Cell
        :return:
        """
        for cell in self.get_cells():
            if cell.was_learning():
                return cell

    def shift_states(self):
        """

        """
        self.previous_state = self.current_state
        self.current_state = ColumnState()

        self.get_connector().reset_active_after_inhibition()

        for cell in self.get_cells():
            cell.shift_states()

    def get_connector(self):
        """

        :rtype: ColumnConnector
        :return:
        """
        return self.__connector

    # logging / debugging
    def get_cell_index(self, cell):
        """

        :param Cell cell:
        :rtype: int
        :return:
        """
        return self.get_cells().index(cell)

    def get_column_index(self):
        """

        :rtype: int
        :return:
        """
        return self.__layer.get_column_index(self)

    def get_cells_amount(self):
        """
        Returns a number of cells in the column.
        :return: int
        """
        return self.__cells_amount
