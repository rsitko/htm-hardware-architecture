from htmproject.core.column import Column
from htmproject.core.spatial_pooler_state import SpatialPoolerState
import math


class Layer(object):
    """
    """
    def __init__(self, htm, config, input_length, columns_amount, cells_per_column):
        self.__config = config
        self.__number_of_synapses_per_column_connector = int(self.__config.get('core.sp.synapses_per_column'))
        # check if a number of columns is well defined i.e.
        # num_of_columns >= 0.5 * (num_inputs / num_of_synapses_per_column)
        threshold = 0.5 * (float(input_length) / self.__number_of_synapses_per_column_connector)
        if columns_amount < threshold:
            raise ValueError("Num. of columns is too low. "
                             "Should follow the formula: "
                             "num_of_columns >= 0.5 * (num_inputs / num_of_synapses_per_column) which is: %s"
                             % (int(math.ceil(threshold)), ))
        self.columns = []
        self.__enable_learning = True
        self.__columns_amount = columns_amount

        self.__htm = htm
        self.__sp_state = SpatialPoolerState(columns_amount)

        self.__enable_tp = cells_per_column > 0

        for i in range(0, columns_amount):
            self.columns.append(Column(self, config, input_length, cells_per_column))

    def get_columns(self):
        """
        :rtype: Column[]
        :return: list of columns
        """

        return self.columns

    def get_columns_amount(self):
        """
        Returns a number of columns in the current layer.
        :return:
        """
        return self.__columns_amount

    def get_current_active_columns(self):
        """

        :rtype: Column[]
        :return:
        """
        active_columns = []

        for column in self.get_columns():
            if column.is_active():
                active_columns.append(column)

        return active_columns

    def get_previous_active_columns(self):
        """

        :rtype: Column[]
        :return:
        """
        active_columns = []

        for column in self.get_columns():
            if column.was_active():
                active_columns.append(column)

        return active_columns

    def toggle_learning(self, state):
        """
        Enable/disable learning of HTM layer.

        :param bool state: True if HTM layer should learn
        """
        self.__enable_learning = state

        # forget queued changes
        if not self.__enable_learning:
            for column in self.get_columns():
                for cell in column.get_cells():
                    cell.clear_queued_updates()

    def is_learning_enabled(self):
        """
        Checks if the HTM layer is currently learning.

        :rtype: bool
        :return:
        """

        return self.__enable_learning

    def is_tp_enabled(self):
        return self.__enable_tp

    def get_previous_learning_cells(self):
        """

        :rtype: Cell[]
        :return:
        """
        learning_cells = []
        for column in self.get_previous_active_columns():
            cell = column.get_previous_learning_cell()
            if cell is not None:
                learning_cells.append(cell)

        return learning_cells

    def get_current_learning_cells(self):
        """

        :rtype: Cell[]
        :return:
        """
        learning_cells = []
        for column in self.get_current_active_columns():
            cell = column.get_current_learning_cell()
            if cell is not None:
                learning_cells.append(cell)

        return learning_cells

    def shift_states(self):
        """

        """
        for column in self.get_columns():
            column.shift_states()

    def get_sp_state(self):
        """

        :rtype: SpatialPoolerState
        :return:
        """
        return self.__sp_state

    def get_column_connectors(self):
        """

        :rtype: ColumnConnector[]
        :return:
        """
        return [c.get_connector() for c in self.get_columns()]

    # logging / debugging
    def get_column_index(self, column):
        """

        :param Column column:
        :return:
        """
        return self.columns.index(column)

    def get_layer_index(self):
        """

        :return:
        """
        return self.__htm.get_layer_index(self)
