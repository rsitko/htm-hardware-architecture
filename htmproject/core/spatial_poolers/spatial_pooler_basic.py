import logging

import math
import numpy


class SpatialPoolerBasic(object):
    """

    """

    def __init__(self, config):
        self._logger = logging.getLogger()
        self._winners_set_size = int(config.get('core.sp.winners_set_size'))
        self._min_overlap = int(config.get('core.sp.min_overlap'))
        self._number_of_synapses = int(config.get('core.sp.synapses_per_column'))

    def update_layer_state(self, layer, item):
        """function update_layer_state

        returns
        """
        column_connectors = layer.get_column_connectors()

        self._inhibition(layer, column_connectors, item)

        for column_connector in column_connectors:
            column_connector.get_column().set_active(column_connector.is_active_after_inhibition())

        if layer.is_learning_enabled():
            self._learning(layer, column_connectors, item)

    @staticmethod
    def get_layer_output(layer):
        """
        Return active columns from a current layer.
        :param layer:
        :return numpy.array:
        """
        return numpy.array([c.is_active() for c in layer.get_columns()])

    def _get_columns_before_inhibition(self, column_connectors, item):
        """

        :param column_connectors:
        :param item:
        :return:
        """
        columns_boosted_overlaps = []
        for connector in column_connectors:
            connector.calculate_overlap(item)
            columns_boosted_overlaps.append(connector.get_boosted_overlap())

        return numpy.array(columns_boosted_overlaps)

    def _inhibition(self, layer, column_connectors, item):
        """

        """
        sp_state = layer.get_sp_state()
        inhibition_radius = sp_state.get_inhibition_radius()
        minus_inhibition_range_2 = int(-math.floor(inhibition_radius/2.0))
        inhibition_range_2_open = minus_inhibition_range_2 + inhibition_radius
        columns_boosted_overlaps = self._get_columns_before_inhibition(column_connectors, item)

        number_of_columns = len(column_connectors)
        threshold = inhibition_radius - self._winners_set_size
        if threshold <= 0:
            for ci in range(0, number_of_columns):
                if columns_boosted_overlaps[ci]:
                    column_connectors[ci].set_active_after_inhibition()
        else:
            for ci in range(0, number_of_columns):
                if columns_boosted_overlaps[ci]:
                    counter = 0
                    for i in range(minus_inhibition_range_2, inhibition_range_2_open):
                        index = (ci + i + number_of_columns) % number_of_columns
                        if columns_boosted_overlaps[ci] > columns_boosted_overlaps[index]:
                            counter += 1
                    if counter > threshold:
                        column_connectors[ci].set_active_after_inhibition()

    @staticmethod
    def _learning(layer, column_connectors, item):
        """
        This function performs all the learning steps. Updates permValues.

        :return:
        """
        sp_state = layer.get_sp_state()
        inhibition_radius = sp_state.get_inhibition_radius()

        for column_connector in column_connectors:
            if column_connector.is_active_after_inhibition():
                column_connector.update_synapses(item)  # change permValues
            column_connector.update_active_duty_cycle()

        # compute maxDutyCycle and minDutyCycle
        column_list = range(len(column_connectors))

        # TODO: change this to RingBuffer
        for i in range(0, len(column_connectors)):
            # iterate over the inhibition radius range and find max. active duty cycle
            neighbouring_max_active_duty_cycle_values = []
            shift_step_0 = i
            shift_step_1 = i + inhibition_radius + 1

            rotated_column_list_left = column_list[shift_step_0:] + column_list[:shift_step_0]
            restricted_rotated_column_list_left = rotated_column_list_left[:inhibition_radius]

            rotated_column_list_right = column_list[shift_step_1:] + column_list[:shift_step_1]
            restricted_rotated_column_list_right = rotated_column_list_right[:inhibition_radius]

            restricted_column = restricted_rotated_column_list_left + [rotated_column_list_left[inhibition_radius-1]]
            restricted_column = restricted_column + restricted_rotated_column_list_right

            for j in restricted_column:
                neighbouring_max_active_duty_cycle_values.append(column_connectors[j].get_active_duty_cycle())

            column_connectors[i].set_max_and_min_duty_cycle(max(neighbouring_max_active_duty_cycle_values))

        inhibition_radiuses = []

        for column_connector in column_connectors:
            column_connector.update_boost()
            column_connector.update_overlap_duty_cycle()
            column_connector.boost_perm_values()
            column_connector.update_inhibition_radius()
            ir = column_connector.get_inhibition_radius()
            if ir is not None:
                inhibition_radiuses.append(ir)

        sp_state.set_inhibition_radius(int(round(numpy.average(inhibition_radiuses))))

    def get_profile_info(self):
        return None
