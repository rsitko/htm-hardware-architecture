import numpy as np
from profilehooks import profile

from htmproject.core.spatial_poolers import SpatialPoolerBasic
from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException
from htmproject.ocls.sp_overlap import OclSpOverlap


class SpatialPoolerGPUOverlap(SpatialPoolerBasic):
    """

    """
    def __init__(self, *args, **kwargs):
        super(SpatialPoolerGPUOverlap, self).__init__(*args, **kwargs)

        try:
            self.ocl_overlap = OclSpOverlap('sp_overlap', self._min_overlap)
        except NoDeviceOCLException:
            self._logger.warning("There is no hardware accelerator.")
            self._get_columns_before_inhibition = super(SpatialPoolerGPUOverlap, self)._get_columns_before_inhibition

    def _get_columns_before_inhibition(self, column_connectors, item):
        """
        Accelerated.
        :param column_connectors:
        :param item:
        :return:
        """
        over_laps = []
        boost_coeff = []

        for connector in column_connectors:
            synapses = connector.get_synapses()
            boost_coeff.append(connector.get_boost())
            for synapse in synapses:
                over_laps.append(synapse.is_active(item))

        synapses_amount = column_connectors[0].get_synapses_amount()
        return self.ocl_overlap.execute_kernel(
            over_laps,
            boost_coeff,
            synapses_amount
        )

    def get_profile_info(self):
        return self.ocl_overlap.get_profile_info()
