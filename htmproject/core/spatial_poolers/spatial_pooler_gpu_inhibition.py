from htmproject.core.spatial_poolers import SpatialPoolerBasic
from htmproject.interfaces.ocl.exceptions import NoDeviceOCLException
from htmproject.ocls.sp_inhibition import OclSpInhibition


class SpatialPoolerGPUInhibiton(SpatialPoolerBasic):
    """

    """
    def __init__(self, *args, **kwargs):
        super(SpatialPoolerGPUInhibiton, self).__init__(*args, **kwargs)

        try:
            self.ocl_inhibition = \
                OclSpInhibition('sp_inhibition', self._min_overlap,
                                self._number_of_synapses,
                                self._winners_set_size)
        except NoDeviceOCLException:
            self._logger.warning("There is no hardware accelerator.")
            self._inhibition = super(SpatialPoolerGPUInhibiton, self)._inhibition

    def _inhibition(self, layer, column_connectors, item):
        """
        GPU accelerated inhibition.
        """
        sp_state = layer.get_sp_state()
        inhibition_radius = sp_state.get_inhibition_radius()
        number_of_columns = layer.get_columns_amount()

        over_laps = []
        boost_coeff = []

        for connector in column_connectors:
            synapses = connector.get_synapses()
            boost_coeff.append(connector.get_boost())
            for synapse in synapses:
                over_laps.append(synapse.is_active(item))

        active_after_inhibition = self.ocl_inhibition.execute_kernel(
            over_laps,
            boost_coeff,
            inhibition_radius,
            number_of_columns
        )

        for i in range(len(active_after_inhibition)):
            if active_after_inhibition[i]:
                column_connectors[i].set_active_after_inhibition()

    def get_profile_info(self):
        return self.ocl_inhibition.get_profile_info()
