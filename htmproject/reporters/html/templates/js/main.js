/* ==========================================================================

    Project: HTML HTML Logger Structure
    Author: XHTMLized
    Last updated: @@timestamp

   ========================================================================== */

(function($) {

  'use strict';

  var App = {

    DOM: {},

    /**
     * Init Function
     */
    init: function() {
      App.DOM.$doc = $(document);
      App.DOM.$content = $('#content');

      App.accordion.init();
      App.visualisation.init();
      App.gotoIteration.init();
    },

    /**
     * Accordion
     */
    accordion: {
      init: function () {
        App.DOM.$doc
          .off('.accordion')
          .on(
            'click.accordion',
            '.accordion-handle',
            App.accordion.events.toggle
          )
          .on(
            'click.accordion',
            '[data-toggle="accordion-collapse"]',
            App.accordion.events.collapseAll
          );
      },

      events: {
        toggle: function () {
          var $acc = $(this).closest('.accordion');
          if ($acc.attr('open')) {
            $acc.removeAttr('open');
          } else {
            $acc.attr('open', true);
          }
        },

        collapseAll: function () {
          $('.accordion[open], .details[open]').removeAttr('open');
        }
      }
    },

    /**
     * Visualisation
     */
    visualisation: {
      init: function () {
        App.DOM.$doc
          .off('.visualisation')
          .on(
            'click.visualisation',
            '.visualisation-table a',
            App.visualisation.events.openAccordions
          );
      },

      events: {
        openAccordions: function (ev) {
          var $target = $($(this).attr('href'));

          $target.parents('.details, .accordion').attr('open', true);

          setTimeout(function () {
            App.DOM.$content.scrollTop(App.DOM.$content.scrollTop() + Math.floor($target.offset().top) - 55);
          }, 100);
        }
      }
    },

    gotoIteration: {
      init: function () {
        App.DOM.$doc
          .off('.goto')
          .on(
            'submit.goto',
            '[data-toggle="goto-iteration"]',
            App.gotoIteration.events.submit
          );
      },

      events: {
        submit: function (ev) {
          ev.preventDefault();

          var index = parseInt($(this).find('input[type="number"]').val(), 10);
          window.location.href = window.location.href.substr(0, window.location.href.lastIndexOf('/') + 1) + 'iteration-' + index + '.html';
        }
      }
    }

  };

  $(function() {
    App.init();
  });

})(jQuery);
